# README #


### What is this repository for? ###

* This is sample project to show 3 type of data (New Episode, Channel and Category) from server using VIPER architecture and SOLID principles with unit test.
* Version 1.0 

### How do I get set up? ###

* Just clone this repository. All are in master branch. And open this project with Xcode
* Dependencies - No third party library or depency used in this project 

### Challenges  ###
* Working without any library and Implementing design pattern (SOLID principle, VIPER) with Unit Testing was a bit difficult. I had to make propar plan for  implementing these, which is time consuming.

### Future Development  ###
* I will add details screen of each item (New Episode, Channel, Category item)  to fecth all of is content and related data. Also we can add live video streaming to take live class and course

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
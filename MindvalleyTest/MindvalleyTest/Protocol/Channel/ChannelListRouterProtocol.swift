//
//  MovieListRouterProtocol.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

//MARK: ChannelListRouterProtocol
/// This protocols  is used as a presenter-router  communcation model
public protocol ChannelListRouterProtocol : class{
    static func creatChannelListModule(channelListViewController : ChannelListViewController) -> Void  
}

//
//  MovieListPresenterDelegate.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

//MARK: ChannelListViewProtocol
/// This protocols  is used as a presenter-view communcation model
public protocol ChannelListViewProtocol : class{

    /// This method is used to receive call back from presenter when view did load
    func initialize()
    
    /// This method is used to receive  new episode from  presenter  after  loading from server or cache
    /// - Parameter newEpisodeList: List of new episode
    func didNewEpisodeDataLoaded(newEpisodeList:[NewEpisode])
    
    /// This method is used to receive  channel data from  presenter  after  loading from server or cache
    /// - Parameter channelList: List of channel
    func didChannelDataLoaded(channelList: [Channel])
    
    /// This method is used to receive category data from  presenter  after  loading from server or cache
    /// - Parameter categoryList: List of category
    func didCategoryDataLoaded(categoryList: [Category])
    
    /// This method is used to receive  callback  from  presenter  after  reset all channel  data
    func didResetAllChannelData()
}

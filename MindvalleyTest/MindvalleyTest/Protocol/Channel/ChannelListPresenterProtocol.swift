//
//  MovieListPresenterProtocol.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

//MARK: ChannelListPresenterProtocol
/// This protocols  is used as a view-presenter, interactor-presenter  communcation model
public protocol ChannelListPresenterProtocol : class{
    var newEpisodeList: [NewEpisode]? {get set}
    var channelList: [Channel]? {get set}
    var categoryList: [Category]? {get set}
    
    var interactor: ChannelListInteractorProtocol? {get set}
    var view: ChannelListViewProtocol? {get set}
    var router: ChannelListRouterProtocol? {get set}
    
    //when view did load initialized
    func viewDidLoad()
    
    //when loading all new episode, series, course and category data
    func loadAllChannelData()
    
    
    /// This method is used to communicate with interactor to load  new episode data
    func loadNewEpisodeData()
    
    /// This method is used to communicate with interactor to load  channel data
    func loadChannelData()
    
    /// This method is used to communicate with interactor to load Category data
    func loadCategoryData()
    
    
    /// This method is used to get new episode from the list depending on position
    /// - Parameter position: position of the popular movie in the list
    func getNewEpisode(at position: Int) -> NewEpisode?
      
    /// This method is used to get channel from the list depending on position
    /// - Parameter position: position of the top rated movie in the list
    func getChannel(at position: Int) -> Channel?
      
    /// This method is used to get Category from the list depending on position
    /// - Parameter position: position of the upcomming movie in the list
    func getCategory(at position: Int) -> Category?
    
    /// This method is used to reset all channel data
    func resetAllChannelData()
}

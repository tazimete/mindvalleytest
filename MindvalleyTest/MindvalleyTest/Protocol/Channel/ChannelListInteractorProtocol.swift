//
//  MovieListInteractorProtocol.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

//MARK: ChannelListInteractorProtocol
/// This protocols  is used as a presenter-interactor  communcation model
public protocol ChannelListInteractorProtocol : class{
    var input: ChannelListInteractorInputProtocol? {get set}
    
    /// This method is used to receive call  from presenter for loading all channel data
    func loadAllChannelData()
    
    /// This method is used to call  from presenter to load new episode  data
    func loadNewEpisodeData()
    
    /// This method is used to call  from presenter to load channel data
    func loadChannelData()
    
    /// This method is used to call  from presenter to load category data
    func loadCategoryData()
    
    /// This method is used to reset all channel data
    func resetAllChannelData()
}


//MARK: ChannelListInteractorInputProtocol
/// This protocols  is used as a data  loader  from server  as input
public protocol ChannelListInteractorInputProtocol : class{
    var newEpisodeList: [NewEpisode]? {get set}
    var channelList: [Channel]? {get set}
    var categoryList: [Category]? {get set}
    var apiClient:Requestable? {get set}
    var ouput: ChannelListInteractorOutputProtocol? {get set}
    
    /// This method is used to receive call  from interactor for loading all channel data
    func loadAllChannelData()
    
    /// This method is used to call  from interactor to load new episode  data
    func loadNewEpisodeData()
    
    /// This method is used to call  from interactor to load channel data
    func loadChannelData()
    
    /// This method is used to call  from interactor to load category data
    func loadCategoryData()
    
    /// This method is used to reset all channel data
    func resetAllChannelData()
}


//MARK: ChannelListInteractorOutputProtocol
/// This protocols  is used as a data  sender  from interactor input  to presenter as output (Its also a presenter)
public protocol ChannelListInteractorOutputProtocol : class{
    
    /// This method is used to recieve new episode list from interactor input   after loading from server or cache
    /// - Parameter newEpisodeList: list of new episodes comes from server
    func didNewEpisodeDataLoaded(newEpisodeList:[NewEpisode])
    
    /// This method is used to recieve channel list from interactor input   after loading from server or cache
    /// - Parameter channelList: list of  channel  comes from server or cache
    func didChannelDataLoaded(channelList: [Channel])
    
    /// This method is used to recieve category list from interactor input   after loading from server or cache
    /// - Parameter categoryList: list of caetgory comes from server or cache
    func didCategoryDataLoaded(categoryList: [Category])
}

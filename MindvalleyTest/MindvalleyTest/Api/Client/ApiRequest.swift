//
//  ApiRequest.swift
//   Mindvalley-Test
//
//  Created by Mostafizur Rahman on 28/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

public class ApiRequest : NSObject, Requestable{
    public let CLASS_NAME = ApiRequest.self.description()
    public static let shared = ApiRequest()
    
    public let allowedDiskSize = 200 * 1024 * 1024
    public lazy var cache: URLCache = {
        return URLCache(memoryCapacity: 0, diskCapacity: allowedDiskSize, diskPath: "mindvalley_test_cache")
    }()

    typealias DownloadCompletionHandler = (Result<Data,Error>) -> ()

    public func createURLSession() -> URLSession {
        let sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.requestCachePolicy = .returnCacheDataElseLoad
        sessionConfiguration.urlCache = cache
        return URLSession(configuration: sessionConfiguration)
    }
    
    //get method
    public  func get(url:String, param:String, completeionHandler: @escaping (_ response:NSDictionary?) -> Void) -> Void{
//        let session = URLSession.shared
        guard let url = URL(string: "\(url)?\(param)") else{
            print("Invalid Url !!")
            return
        }
        
        var request = URLRequest(url: url, cachePolicy: .returnCacheDataElseLoad, timeoutInterval: 20)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

//        let task = session.dataTask(with: request) { data, response, error in
        createURLSession().dataTask(with: request) { (data, response, error) in

            print("ApiRequest -- get(\(url)) -- data = \(data), response = \(response), error = \(error)")
            

            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error!")
                
                if let cachedData = self.cache.cachedResponse(for: request) {
                    print("Cached data in bytes:", cachedData.data)
                   let result = try? JSONSerialization.jsonObject(with: cachedData.data, options: [])
                    
                    completeionHandler(result as? NSDictionary)
                }
                
                
                return
            }
            
            var cachedData = CachedURLResponse(response: response, data: data!)
            cachedData = cachedData.response(withExpirationDuration: 10000)
            self.cache.storeCachedResponse(cachedData, for: request)
            
//            let result = try? JSONSerialization.jsonObject(with: responseData, options: [])
            let result = try? JSONSerialization.jsonObject(with: cachedData.data, options: [])
            
            completeionHandler(result as? NSDictionary)
        }.resume()
    }
    
    
    public func getChannelData() -> [Channel] {
        return [Channel]()
    }
       
    public func getNewEpisodeData() -> [NewEpisode] {
        return [NewEpisode]()
    }
       
    public func getCategoryData() -> [Category] {
        return [Category]()
    }
}



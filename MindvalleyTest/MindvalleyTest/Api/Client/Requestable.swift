//
//  ApiRequestable.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 28/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

public protocol Requestable: class {
    func get(url:String, param:String, completeionHandler: @escaping (_ response:NSDictionary?) -> Void) -> Void
    
    func getChannelData() -> [Channel]
    func getNewEpisodeData() -> [NewEpisode]
    func getCategoryData() -> [Category]
}

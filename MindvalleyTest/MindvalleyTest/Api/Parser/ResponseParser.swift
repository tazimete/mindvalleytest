//
//  Parser.swift
//   Mindvalley-Test
//
//  Created by Mostafizur Rahman on 29/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit
import CoreData

class ResponseParser {
    //pasre new episode api response
    public static func parseNewEpisodeApiResponse(response:NSDictionary?) -> [NewEpisode]{
        var newEpisodeList = [NewEpisode]()
        let newEpisodeJsonList = response?.value(forKeyPath: "data.media") as? [NSDictionary]
        
        for newEpisodeJson in newEpisodeJsonList ?? [NSDictionary](){
            let newEpisode = NewEpisode()
            newEpisode.title = newEpisodeJson["title"] as? String
            newEpisode.coverPicture = (newEpisodeJson["coverAsset"] as? NSDictionary)?["url"] as? String
            newEpisode.type = newEpisodeJson["type"] as? String
            newEpisode.channelName = (newEpisodeJson["channel"] as? NSDictionary)?["title"] as? String

            newEpisodeList.append(newEpisode)
        }

        return newEpisodeList
    }

    
    //pasre channel api response
    public static func parseChannelApiResponse(response:NSDictionary?) -> [Channel]{
        var channelList = [Channel]()
        let channelJsonList = response?.value(forKeyPath: "data.channels") as? [NSDictionary]
        
        for channelJson in channelJsonList ?? [NSDictionary](){
            let channel = Channel()
            channel.id = channelJson["id"] as? String
            channel.title = channelJson["title"] as? String
            channel.iconUrl = (channelJson["iconAsset"] as? NSDictionary)?["thumbnailUrl"] as? String
            channel.coverUrl = (channelJson["coverAsset"] as? NSDictionary)?["url"] as? String
            
            //parse series
            var seriesList = [Series]()
            let seriesJsonList = (channelJson["series"] as? [NSDictionary]) ?? [NSDictionary]()
            
            for seriesJson in seriesJsonList{
                let series = Series()
                series.title = seriesJson["title"] as? String
                series.channelName = channelJson["title"] as? String
                series.coverPicture = (seriesJson["coverAsset"] as? NSDictionary)?["url"] as? String
                
                seriesList.append(series)
            }
            
            channel.seriesList = seriesList
            
            //parse course
            var courseList = [Course]()
            let courseJsonList = (channelJson["latestMedia"] as? [NSDictionary]) ?? [NSDictionary]()
            
            for courseJson in courseJsonList{
                let course = Course()
                course.title = courseJson["title"] as? String
                course.channelName = channelJson["title"] as? String
                course.coverPicture = (courseJson["coverAsset"] as? NSDictionary)?["url"] as? String
                course.type = courseJson["type"] as? String
                
                courseList.append(course)
            }
            
            channel.courseList = courseList
    
            //append new channel to ther list
            channelList.append(channel)
        }

        return channelList
    }

    //pasre category api respose
    public static func parseCategoryApiResponse(response:NSDictionary?) -> [Category]{
        var categoryList = [Category]()
        let categoryJsonList = response?.value(forKeyPath: "data.categories") as? [NSDictionary]
        
        for categoryJson in categoryJsonList ?? [NSDictionary](){
            let category = Category()
            category.name = categoryJson["name"] as? String

            categoryList.append(category)
        }

        return categoryList
    }
}

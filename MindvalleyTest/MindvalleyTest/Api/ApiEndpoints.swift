//
//  ApiEndpoints.swift
//  Mindvalley-Test
//
//  Created by AGM Tazim on 9/3/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class ApiEndpoints: NSObject {

    public static let BASE_URL_API:String = ApiManager.API_SERVER_BASE_URL
    
    //Bew Episode
    public static let URI_GET_NEW_EPISODE_LIST: String = BASE_URL_API+"/raw/z5AExTtw"
    
    //Channel
    public static let URI_GET_CHANNEL_LIST: String = BASE_URL_API+"/raw/Xt12uVhM"
    
    //Category 
    public static let URI_GET_CATEGORY_LIST: String = BASE_URL_API+"/raw/A0CgArX3"
    
}

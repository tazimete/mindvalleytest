//
//  ChannelListRouter.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 28/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

class ChannelListRouter: ChannelListRouterProtocol {
    //create channel list module
    static func creatChannelListModule(channelListViewController : ChannelListViewController) -> Void {
        let presenter: ChannelListPresenterProtocol = ChannelListPresenter()
        
        channelListViewController.presenter = presenter
        channelListViewController.presenter?.router = ChannelListRouter()
        channelListViewController.presenter?.view = channelListViewController
        channelListViewController.presenter?.interactor = ChannelListInteractor()
        channelListViewController.presenter?.interactor?.input = ChannelListInteractorInput()
        channelListViewController.presenter?.interactor?.input?.apiClient = ApiRequest.shared
        channelListViewController.presenter?.interactor?.input?.ouput = presenter as? ChannelListInteractorOutputProtocol
    }
}

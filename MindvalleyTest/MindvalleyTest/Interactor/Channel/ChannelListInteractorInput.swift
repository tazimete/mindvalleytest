//
//  ChannelListInteractorInput.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 28/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

class ChannelListInteractorInput: NSObject, ChannelListInteractorInputProtocol {
    public let CLASS_NAME = ChannelListInteractorInput.self.description()
    public var newEpisodeList: [NewEpisode]?
    public var channelList: [Channel]?
    public var categoryList: [Category]?
    public var apiClient: Requestable?
    public var ouput: ChannelListInteractorOutputProtocol?
    
    /// This method is used to receive call  from interactor for loading all channel data
    public func loadAllChannelData(){
        loadNewEpisodeData()
        loadChannelData()
        loadCategoryData()
    }
    
    /// This method is used to call  from interactor to load new episode  data
    public func loadNewEpisodeData(){
        apiClient?.get(url: ApiEndpoints.URI_GET_NEW_EPISODE_LIST, param: "", completeionHandler: {
            [weak self] response in
            
            print("\(self?.CLASS_NAME) -- loadNewEpisodeData() -- response = \(response)")
            
            self?.newEpisodeList = ResponseParser.parseNewEpisodeApiResponse(response: response)
            
            self?.ouput?.didNewEpisodeDataLoaded(newEpisodeList: self?.newEpisodeList ?? [NewEpisode]())
        })
    }
    
    /// This method is used to call  from interactor to load channel data
    public func loadChannelData(){
        apiClient?.get(url: ApiEndpoints.URI_GET_CHANNEL_LIST, param: "", completeionHandler: {
            [weak self] response in
            
            print("\(self?.CLASS_NAME) -- loadChannelData() -- response = \(response)")
            
            self?.channelList = ResponseParser.parseChannelApiResponse(response: response)
            
            self?.ouput?.didChannelDataLoaded(channelList: self?.channelList ?? [Channel]())
        })
    }
    
    /// This method is used to call  from interactor to load category data
    public func loadCategoryData(){
        apiClient?.get(url: ApiEndpoints.URI_GET_CATEGORY_LIST, param: "", completeionHandler: {
            [weak self] response in
            
            print("\(self?.CLASS_NAME) -- loadCategoryData() -- response = \(response)")
            
            self?.categoryList = ResponseParser.parseCategoryApiResponse(response: response)
            
            self?.ouput?.didCategoryDataLoaded(categoryList: self?.categoryList ?? [Category]())
        })
    }
    
    /// This method is used to reset all channel data
    public func resetAllChannelData(){
        newEpisodeList?.removeAll()
        channelList?.removeAll()
        categoryList?.removeAll()
    }
}

//
//  ChannelListInteractor.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 28/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

class ChannelListInteractor: NSObject, ChannelListInteractorProtocol {
    public let CLASS_NAME = ChannelListInteractor.self.description()
    
    public var input: ChannelListInteractorInputProtocol?
    
    /// This method is used to receive call  from presenter for loading all channel data
    public func loadAllChannelData(){
        input?.loadAllChannelData()
    }
    
    /// This method is used to call  from presenter to load new episode  data
    public func loadNewEpisodeData(){
        input?.loadNewEpisodeData()
    }
    
    /// This method is used to call  from presenter to load channel data
   public func loadChannelData(){
        input?.loadChannelData()
    }
    
    /// This method is used to call  from presenter to load category data
    public func loadCategoryData(){
        input?.loadCategoryData()
    }
    
    /// This method is used to reset all channel data
    public func resetAllChannelData(){
        input?.resetAllChannelData()
    }
}

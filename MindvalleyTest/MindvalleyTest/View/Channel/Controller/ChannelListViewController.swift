//
//  ViewController.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 23/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

public class ChannelListViewController: UIViewController {
    //MARK: Properties
    public let CLASS_NAME = ChannelListViewController.self.description()
    public var presenter:ChannelListPresenterProtocol?
    private var cacheCellheightList = [CGFloat]()
    
    public override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    //MARK: Outlets
    @IBOutlet weak var cvChannelList: UICollectionView!
    
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        //initialize
        ChannelListRouter.creatChannelListModule(channelListViewController: self)
        presenter?.viewDidLoad()
        
        // load all channel  data
        presenter?.loadAllChannelData()
        
    }

    //init navigation bar
     private func initNavigationBar() -> Void {
        self.navigationItem.title = "Channel"
        
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: ColorCollection.APP_THEME_LIGHT_GRAY_COLOR]
        } else {
            // Fallback on earlier versions
        }
        
        self.navigationController?.navigationBar.barTintColor = ColorGenerator.UIColorFromHex(rgbValue: ColorCollection.APP_THEME_DARK_GRAY_COLOR_CODE, alpha: 0.5)
//        self.navigationController?.navigationBar.barTintColor = .clear
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: ColorCollection.APP_THEME_LIGHT_GRAY_COLOR]
        self.view.backgroundColor = ColorCollection.APP_THEME_DARK_GRAY_COLOR
     }
     
     //initializeListView
    private func initializeListView() -> Void {
         //init collectionviews
         cvChannelList.delegate = self
         cvChannelList.dataSource = self
         
         //set collection view cell item cell
         let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
         layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
         layout.minimumInteritemSpacing = 5
         layout.minimumLineSpacing = 5
         layout.scrollDirection = .vertical
         cvChannelList.collectionViewLayout = layout
         
         //cell for food list collection view
         cvChannelList?.register(UINib(nibName: "NewEpisodeSectionCell", bundle: nil), forCellWithReuseIdentifier: "NewEpisodeSectionCell")
         cvChannelList?.register(UINib(nibName: "ChannelSeriesSectionCell", bundle: nil), forCellWithReuseIdentifier: "ChannelSeriesSectionCell")
         cvChannelList?.register(UINib(nibName: "ChannelCourseSectionCell", bundle: nil), forCellWithReuseIdentifier: "ChannelCourseSectionCell")
         cvChannelList?.register(UINib(nibName: "ChannelCategorySectionCell", bundle: nil), forCellWithReuseIdentifier: "ChannelCategorySectionCell")
    }
    
    //reload collectionview
    private func reloadCollectionView() -> Void {
        self.cacheCellheightList.removeAll()
        self.cvChannelList.reloadData()
    }

}


//MARK: ChannelListViewProtocol
/// This protocols  is used as a presenter-view communcation model
extension ChannelListViewController : ChannelListViewProtocol{
    /// This method is used to receive call back from presenter when view did load
    public func initialize(){
        initNavigationBar()
        initializeListView()
    }
    
    /// This method is used to receive  new episode from  presenter  after  loading from server or cache
    /// - Parameter newEpisodeList: List of new episode
    public func didNewEpisodeDataLoaded(newEpisodeList:[NewEpisode]){
        DispatchQueue.main.async {[weak self] in
            self?.reloadCollectionView()
        }
    }
    
    /// This method is used to receive  channel data from  presenter  after  loading from server or cache
    /// - Parameter channelList: List of channel
    public func didChannelDataLoaded(channelList: [Channel]){
        DispatchQueue.main.async { [weak self] in
            self?.reloadCollectionView()
        }
    }
    
    /// This method is used to receive category data from  presenter  after  loading from server or cache
    /// - Parameter categoryList: List of category
    public func didCategoryDataLoaded(categoryList: [Category]){
        DispatchQueue.main.async { [weak self] in
            self?.reloadCollectionView()
        }
    }
    
    /// This method is used to receive  callback  from  presenter  after  reset all channel  data
    public func didResetAllChannelData(){
        self.reloadCollectionView()
    }
}


//MARK: UICollectionView
extension ChannelListViewController:  UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (presenter?.channelList?.count ?? 0)+2
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height:CGFloat = 0
        
        //cache cell ehight list
        if indexPath.row < cacheCellheightList.count {
             height = cacheCellheightList[indexPath.row]
            return CGSize(width: cvChannelList.frame.width, height: CGFloat(height))
        }
        
        let newEpisodeList = presenter?.newEpisodeList ?? [NewEpisode]()
        let channelList = presenter?.channelList ?? [Channel]()
        let categoryList = presenter?.categoryList ?? [Category]()
        
        if indexPath.row == 0{
            if newEpisodeList.count > 0{
                height = 50+305+Helper.getMaxHeight(width: 152, values: newEpisodeList.map({$0.title ?? ""}), font: UIFont(name: "Roboto-Bold", size: 17)!)
            }
        }else if indexPath.row > channelList.count{
            if categoryList.count > 0{
                height = CGFloat(((60+16)*((categoryList.count/2)+categoryList.count%2)+85))
            }
        }else  if (channelList[indexPath.row-1].seriesList?.count ?? 0) > 0{
            if channelList.count > 0{
                height = 94+207+Helper.getMaxHeight(width: 320, values: channelList[indexPath.row-1].seriesList?.map({$0.title ?? ""}) ?? [String](), font: UIFont(name: "Roboto-Bold", size: 17)!)
            }
        }else if (channelList[indexPath.row-1].courseList?.count ?? 0) > 0{
            if channelList.count > 0{
                height = 94+263+Helper.getMaxHeight(width: 152, values: channelList[indexPath.row-1].courseList?.map({$0.title ?? ""}) ?? [String](), font: UIFont(name: "Roboto-Bold", size: 17)!)
            }
        }
        
        cacheCellheightList.append(height)
        
        return CGSize(width: cvChannelList.frame.width, height: CGFloat(height))
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let newEpisodeList = presenter?.newEpisodeList ?? [NewEpisode]()
        let channelList = presenter?.channelList ?? [Channel]()
        let categoryList = presenter?.categoryList ?? [Category]()
        
//        var height:CGFloat = 0
//
//        if indexPath.row == 0{
//            if newEpisodeList.count > 0{
//                height = 50+305+Helper.getMaxHeight(width: 152, values: newEpisodeList.map({$0.title ?? ""}), font: UIFont(name: "Roboto-Bold", size: 17)!)
//            }
//        }else if indexPath.row > channelList.count{
//            if categoryList.count > 0{
//                height = CGFloat(((60+16)*((categoryList.count/2)+categoryList.count%2)+85))
//            }
//        }else  if (channelList[indexPath.row-1].seriesList?.count ?? 0) > 0{
//            if channelList.count > 0{
//                height = 94+207+Helper.getMaxHeight(width: 320, values: channelList[indexPath.row-1].seriesList?.map({$0.title ?? ""}) ?? [String](), font: UIFont(name: "Roboto-Bold", size: 17)!)
//            }
//        }else if (channelList[indexPath.row-1].courseList?.count ?? 0) > 0{
//            if channelList.count > 0{
//                height = 94+263+Helper.getMaxHeight(width: 152, values: channelList[indexPath.row-1].courseList?.map({$0.title ?? ""}) ?? [String](), font: UIFont(name: "Roboto-Bold", size: 17)!)
//            }
//        }
//
//        cacheCellheightList.append(height)
        
        
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewEpisodeSectionCell", for: indexPath) as! NewEpisodeSectionCell
            
            cell.height = cacheCellheightList[indexPath.row]-50
            cell.newEpisodeList = newEpisodeList
            
            return cell
        }else if indexPath.row > channelList.count{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChannelCategorySectionCell", for: indexPath) as! ChannelCategorySectionCell
            
            cell.categoryList = categoryList
            
            return cell
        }else  if (channelList[indexPath.row-1].seriesList?.count ?? 0) > 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChannelSeriesSectionCell", for: indexPath) as! ChannelSeriesSectionCell
            
            cell.height = cacheCellheightList[indexPath.row]-94
            
            let channel = channelList[indexPath.row-1]
            cell.lblSectionTitle.text = channel.title
            cell.lblNoOfEpisode.text = "\(channel.seriesList?.count ?? 0) series"
            cell.ivChannelIcon.loadImageFromUrl(fromURL: channel.iconUrl)
            cell.seriesList = channel.seriesList
            
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChannelCourseSectionCell", for: indexPath) as! ChannelCourseSectionCell
            
            cell.height = cacheCellheightList[indexPath.row]-94
            
            let channel = channelList[indexPath.row-1]
            cell.lblSectionTitle.text = channel.title
            cell.lblNoOfEpisode.text = "\(channel.courseList?.count ?? 0) \((channel.courseList?.count ?? 0) < 2 ? "episode":"episodes")"
            cell.ivChannelIcon.loadImageFromUrl(fromURL: channel.iconUrl)
            cell.courseList = channelList[indexPath.row-1].courseList
            
            return cell
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let lastIndexPath = collectionView.indexPathsForVisibleItems.first{
            if lastIndexPath.row <= indexPath.row{
                cell.center.y = cell.center.y + cell.frame.height / 2
                cell.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0, options: [.curveEaseInOut], animations: {
                    cell.center.y = cell.center.y - cell.frame.height / 2
                    cell.alpha = 1
                }, completion: nil)
            }
        }
    }
}

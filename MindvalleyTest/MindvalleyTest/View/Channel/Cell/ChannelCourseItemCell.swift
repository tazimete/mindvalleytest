//
//  ChannelSeriesItemCell.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 23/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

class ChannelCourseItemCell: UICollectionViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var uivContainer: UIView!
    @IBOutlet weak var ivSeriesImage: UIImageView!
    @IBOutlet weak var lblSeriesTitle: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblSeriesTitle.font = UIFont(name: "Roboto-Bold", size: 17)!
    }
    
    override func prepareForReuse() {
        ivSeriesImage.image = nil
        ivSeriesImage.kf.cancelDownloadTask()
    }

}

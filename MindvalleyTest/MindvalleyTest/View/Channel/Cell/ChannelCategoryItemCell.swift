//
//  ChannelCategoryItemCell.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 26/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

class ChannelCategoryItemCell: UICollectionViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var uivContainer: UIView!
    @IBOutlet weak var lblCategory: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        uivContainer.backgroundColor = ColorGenerator.UIColorFromHex(rgbValue: 0x3B3F45)
        uivContainer.layer.cornerRadius = 30
    }

}

//
//  ChannelCategorySectionCell.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 26/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

class ChannelCategorySectionCell: UICollectionViewCell{
    
    //MARK: Properties
    public let CLASS_NAME = ChannelCategorySectionCell.self.description()
    public var categoryList:[Category]!{
        didSet{
            self.cvCategoryList.reloadData()
        }
    }
    
    //MARK: Outlets
    @IBOutlet weak var uivContainer: UIView!
    @IBOutlet weak var lblSectionTitle: UILabel!
    @IBOutlet weak var cvCategoryList: UICollectionView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialization()
    }
    
    //MARK: Intsance Method
   //initialization
   public func initialization() -> Void {
       
       //init collectionviews
       cvCategoryList.delegate = self
       cvCategoryList.dataSource = self
       
       //set collection view cell item cell
       let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
       layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
       layout.minimumInteritemSpacing = 2
       layout.minimumLineSpacing = 16
       layout.scrollDirection = .vertical
       cvCategoryList.collectionViewLayout = layout
       
       //cell for food list collection view
       cvCategoryList?.register(UINib(nibName: "ChannelCategoryItemCell", bundle: nil), forCellWithReuseIdentifier: "ChannelCategoryItemCell")
   }

}


//MARK: Collection View
extension ChannelCategorySectionCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height:CGFloat = 60
        return CGSize(width: (cvCategoryList.frame.width/2)-20, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChannelCategoryItemCell", for: indexPath) as! ChannelCategoryItemCell
        
        cell.lblCategory.text = categoryList[indexPath.row].name
        
        return cell
    }
}


//
//  ChannelSectionCell.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 23/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

class ChannelCourseSectionCell: UICollectionViewCell {
    
    //MARK: Properties
    public let CLASS_NAME = ChannelCourseSectionCell.self.description()
    public var height:CGFloat!
    public var courseList:[Course]!{
        didSet{
           self.cvChannelList.reloadData()
        }
    }
    
    
    //MARK: Outlets
    @IBOutlet weak var uivContainer: UIView!
    @IBOutlet weak var ivChannelIcon: UIImageView!
    @IBOutlet weak var lblSectionTitle: UILabel!
    @IBOutlet weak var lblNoOfEpisode: UILabel!
    @IBOutlet weak var cvChannelList: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialization()
    }
    
    //MARK: Intsance Method
   //initialization
   public func initialization() -> Void {
       
       //init collectionviews
       cvChannelList.delegate = self
       cvChannelList.dataSource = self
       
       //set collection view cell item cell
       let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
       layout.sectionInset = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
       layout.minimumInteritemSpacing = 20
       layout.minimumLineSpacing = 20
       layout.scrollDirection = .horizontal
       cvChannelList.collectionViewLayout = layout
       
       //cell for food list collection view
       cvChannelList?.register(UINib(nibName: "ChannelCourseItemCell", bundle: nil), forCellWithReuseIdentifier: "ChannelCourseItemCell")
   }

}


//MARK: Collection View
extension ChannelCourseSectionCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return min(6, courseList.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 152, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChannelCourseItemCell", for: indexPath) as! ChannelCourseItemCell
        
        let course = courseList[indexPath.row]
        cell.lblSeriesTitle.text = course.title
        cell.ivSeriesImage.loadImageFromUrl(fromURL: course.coverPicture)
        
        return cell
    }
    
}


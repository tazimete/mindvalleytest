//
//  NewEpisodItemCell.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 23/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

class NewEpisodItemCell: UICollectionViewCell {

    @IBOutlet weak var uivContainer: UIView!
    @IBOutlet weak var ivEpisodeImage: UIImageView!
    @IBOutlet weak var lblEpisodeDescription: UILabel!
    @IBOutlet weak var lblEpisodeTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblEpisodeDescription.font = UIFont(name: "Roboto-Bold", size: 17)!
        lblEpisodeTitle.font = UIFont(name: "Roboto-Bold", size: 13)!
        ivEpisodeImage.layer.cornerRadius = 8
    }
    
    override func prepareForReuse() {
        ivEpisodeImage.image = nil
        ivEpisodeImage.kf.cancelDownloadTask()
    }

}

//
//  ChannelCourseItemCell.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 23/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

class ChannelSeriesItemCell: UICollectionViewCell {

    //MARK: Outlets
    @IBOutlet weak var uivContainer: UIView!
    @IBOutlet weak var ivCourseImage: UIImageView!
    @IBOutlet weak var lblCourseTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblCourseTitle.font = UIFont(name: "Roboto-Bold", size: 17)!
    }

    override func prepareForReuse() {
        ivCourseImage.image = nil
        ivCourseImage.kf.cancelDownloadTask()
    }
}

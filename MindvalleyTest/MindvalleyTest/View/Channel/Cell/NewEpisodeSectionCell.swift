//
//  NewEpisodSectionCell.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 23/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

class NewEpisodeSectionCell: UICollectionViewCell{
    
    //MARK: Properties
    public let CLASS_NAME = NewEpisodeSectionCell.self.description()
    public var height:CGFloat!
    public var newEpisodeList:[NewEpisode]!{
        didSet{
            self.cvNewEpisodList.reloadData()
        }
    }
    
    //MARK: Outlets
    @IBOutlet weak var uivContainer: UIView!
    @IBOutlet weak var lblSectionTitle: UILabel!
    @IBOutlet weak var cvNewEpisodList: UICollectionView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialization()
    }
    
    //MARK: Intsance Method
   //initialization
   public func initialization() -> Void {

       //init collectionviews
       cvNewEpisodList.delegate = self
       cvNewEpisodList.dataSource = self
       
       //set collection view cell item cell
       let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
       layout.sectionInset = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
       layout.minimumInteritemSpacing = 20
       layout.minimumLineSpacing = 20
       layout.scrollDirection = .horizontal
       cvNewEpisodList.collectionViewLayout = layout
       
       //cell for food list collection view
       cvNewEpisodList?.register(UINib(nibName: "NewEpisodItemCell", bundle: nil), forCellWithReuseIdentifier: "NewEpisodItemCell")
   }

}


//MARK: Collection View
extension NewEpisodeSectionCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return min(6, newEpisodeList.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 152, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewEpisodItemCell", for: indexPath) as! NewEpisodItemCell
        
        let newEpisode = newEpisodeList[indexPath.row]
        cell.lblEpisodeDescription.text = newEpisode.title
        cell.lblEpisodeTitle.text = newEpisode.channelName
        cell.ivEpisodeImage.loadImageFromUrl(fromURL: newEpisode.coverPicture)
        
        return cell
    }
}


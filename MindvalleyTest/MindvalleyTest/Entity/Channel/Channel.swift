//
//  ChannelSeries.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 26/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

public class Channel : Equatable{
    var id:String?
    var title: String?
    var iconUrl: String?
    var coverUrl: String?
    var seriesList:[Series]?
    var courseList:[Course]?
    
    public static func == (lhs: Channel, rhs: Channel) -> Bool {
        return lhs.id == rhs.id && lhs.title == rhs.title && lhs.iconUrl == rhs.iconUrl && lhs.coverUrl == rhs.coverUrl && lhs.seriesList == rhs.seriesList && lhs.courseList == rhs.courseList
    }
}

//
//  Channel.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 26/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

public protocol Episode : class {
    var title:String? {get set}
    var coverPicture:String? {get set}
    var type:String? {get set}
    var channelName:String? {get set}
}

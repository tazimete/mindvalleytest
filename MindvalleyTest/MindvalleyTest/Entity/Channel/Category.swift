//
//  Category.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 27/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

public class Category: Equatable {
    var name: String?
    
    public static func == (lhs: Category, rhs: Category) -> Bool {
        return lhs.name == rhs.name
    }
}

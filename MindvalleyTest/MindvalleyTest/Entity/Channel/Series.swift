//
//  Series.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 27/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

public class Series: Episode, Equatable {
    public var title: String?
    public var coverPicture: String?
    public var type: String?
    public var channelName: String?
    
    public static func == (lhs: Series, rhs: Series) -> Bool {
        return lhs.title == rhs.title && lhs.coverPicture == rhs.coverPicture && lhs.type == rhs.type && lhs.channelName == rhs.channelName
    }
}

//
//  ChannelListPresenter.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 28/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

class ChannelListPresenter: NSObject, ChannelListPresenterProtocol, ChannelListInteractorOutputProtocol {
    public let CLASS_NAME = ChannelListPresenter.self.description()
    var newEpisodeList: [NewEpisode]?
    var channelList: [Channel]?
    var categoryList: [Category]?
    
    var interactor: ChannelListInteractorProtocol?
    var view: ChannelListViewProtocol?
    var router: ChannelListRouterProtocol?
    
    //when view did load initialized
    func viewDidLoad(){
        view?.initialize()
    }
    
    //when loading all new episode, series, course and category data
    func loadAllChannelData(){
        interactor?.loadAllChannelData()
    }
    
    /// This method is used to communicate with interactor to load  new episode data
    func loadNewEpisodeData(){
        interactor?.loadNewEpisodeData()
    }
    
    /// This method is used to communicate with interactor to load  channel data
    func loadChannelData(){
        interactor?.loadChannelData()
    }
    
    /// This method is used to communicate with interactor to load Category data
    func loadCategoryData(){
        interactor?.loadCategoryData()
    }
    
    /// This method is used to recieve new episode list from interactor input   after loading from server or cache
    /// - Parameter newEpisodeList: list of new episodes comes from server
    func didNewEpisodeDataLoaded(newEpisodeList:[NewEpisode]){
        self.newEpisodeList = newEpisodeList
        view?.didNewEpisodeDataLoaded(newEpisodeList: newEpisodeList)
    }
    
    /// This method is used to recieve channel list from interactor input   after loading from server or cache
    /// - Parameter channelList: list of  channel  comes from server or cache
    func didChannelDataLoaded(channelList: [Channel]){
        self.channelList = channelList
        view?.didChannelDataLoaded(channelList: channelList)
    }
    
    /// This method is used to recieve category list from interactor input   after loading from server or cache
    /// - Parameter categoryList: list of caetgory comes from server or cache
    func didCategoryDataLoaded(categoryList: [Category]){
        self.categoryList = categoryList
        view?.didCategoryDataLoaded(categoryList: categoryList)
    }
    
    /// This method is used to get new episode from the list depending on position
    /// - Parameter position: position of the popular movie in the list
    func getNewEpisode(at position: Int) -> NewEpisode?{
        return newEpisodeList?[position]
    }
      
    /// This method is used to get channel from the list depending on position
    /// - Parameter position: position of the top rated movie in the list
    func getChannel(at position: Int) -> Channel?{
        return channelList?[position]
    }
      
    /// This method is used to get Category from the list depending on position
    /// - Parameter position: position of the upcomming movie in the list
    func getCategory(at position: Int) -> Category?{
        return categoryList?[position]
    }
    
    /// This method is used to reset all channel data
    func resetAllChannelData(){
        newEpisodeList?.removeAll()
        channelList?.removeAll()
        categoryList?.removeAll()
        
        interactor?.resetAllChannelData()
    }
}

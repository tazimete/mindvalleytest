//
//  ColorGenerator.swift
//   Mindvalley-Test
//
//  Created by AGM Tazim on 27/7/19.
//  Copyright © 2019 MacBook Pro. All rights reserved.
//

import UIKit


/// This class provides UIColor from rgb color codes
class ColorGenerator: NSObject {
    
    
    /// This methods provides UIColor from rgb color codes
    /// - Parameters:
    ///   - rgbValue: rgbValue  of the color
    ///   - alpha: alpha (Opacity) of the color
    /// - Returns: UIColor object generated from rgb color code
    static func UIColorFromHex(rgbValue:UInt32, alpha:Double=1.0)->UIColor {
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:CGFloat(alpha))
    }
}

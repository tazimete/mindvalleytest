//
//  ColorCollection.swift
//   Mindvalley-Test
//
//  Created by AGM Tazim on 27/7/19.
//  Copyright © 2019 MacBook Pro. All rights reserved.
//

import UIKit


/// This class provides color codes and object 
class ColorCollection: NSObject {
    public static let APP_THEME_DARK_GRAY_COLOR_CODE:UInt32 = 0x23272F 
    public static let APP_THEME_LIGHT_GRAY_COLOR_CODE:UInt32 = 0xC1C1C1
    public static let COLOR_DEEP_GRAY_COLOR_CODE:UInt32 = 0xBABABA
    public static let COLOR_LIGHT_GRAY_COLOR_CODE:UInt32 = 0xF2F2F2
    
    public static let APP_THEME_DARK_GRAY_COLOR:UIColor = ColorGenerator.UIColorFromHex(rgbValue: APP_THEME_DARK_GRAY_COLOR_CODE)
    public static let APP_THEME_LIGHT_GRAY_COLOR:UIColor = ColorGenerator.UIColorFromHex(rgbValue: APP_THEME_LIGHT_GRAY_COLOR_CODE)
    
}

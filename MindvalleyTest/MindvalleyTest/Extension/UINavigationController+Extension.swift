//
//  UINavigationController+Extension.swift
//  MindvalleyTest
//
//  Created by Mostafizur Rahman on 25/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit

extension UINavigationController {
   open override var preferredStatusBarStyle: UIStatusBarStyle {
      return topViewController?.preferredStatusBarStyle ?? .default
   }
}

//
//  UIIMageViw+.swift
//   Mindvalley-Test
//
//  Created by Mostafizur Rahman on 29/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit


public extension UIImageView {
     func loadImageFromURL(fromURL url: String) {
        if image == nil{
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .topLeftBottomRight)
            let gradient = SkeletonGradient(baseColor: UIColor.midnightBlue)
            showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation)
        }
        
        guard let imageURL = URL(string: url) else {
            self.image = UIImage(named: "image loader@4x")
            return
        }
        
//        let cache =  URLCache.shared
        let cache =  ApiRequest.shared.cache
        let request = URLRequest(url: imageURL)
        DispatchQueue.global(qos: .userInitiated).async {
            if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    self.transition(toImage: image)
                }
            } else {
//                URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                ApiRequest.shared.createURLSession().dataTask(with: request, completionHandler: { (data, response, error) in
                    if let data = data, let response = response, ((response as? HTTPURLResponse)?.statusCode ?? 500) < 300, let image = UIImage(data: data) {
                        let cachedData = CachedURLResponse(response: response, data: data)
                        cache.storeCachedResponse(cachedData, for: request)
                        DispatchQueue.main.async {
                            self.transition(toImage: image)
                        }
                    }else{
                        DispatchQueue.main.async {
                            self.image = UIImage(named: "image loader@4x")
                        }
                    }
                }).resume()
            }
        }
    }
    
     func transition(toImage image: UIImage?) {
        UIView.transition(with: self, duration: 0.3,
                          options: [.transitionCrossDissolve],
                          animations: {
//                            let resizedImage = Helper.resizeImageToFitView(image: image!, maxWidth: self.frame.width, maxHeight: self.frame.height)
                            self.image = image
        },
                          completion: {
                            isCompleted in
                            self.hideSkeleton()
        })
    }
    
    
    //load image
    func loadImageFromUrl(fromURL url: String?) -> Void{
        if image == nil{
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .topLeftBottomRight)
            let gradient = SkeletonGradient(baseColor: UIColor.midnightBlue)
            showAnimatedGradientSkeleton(usingGradient: gradient, animation: animation)
        }
        
        guard let imageURL = URL(string: url ?? "") else {
            self.image = UIImage(named: "image loader@4x")
            return
        }
        
        self.kf.setImage(with: imageURL, options: [.processor(DownsamplingImageProcessor(size:self.bounds.size)), .scaleFactor(UIScreen.main.scale)], completionHandler: {
            (image, error, cacheType, imageUrl) in
            
            if error == nil{
//                self.transition(toImage: image)
                self.image = image
                 self.hideSkeleton()
            }else{
//                self.transition(toImage: UIImage(named: "image loader@4x"))
            }
        })
    }
}

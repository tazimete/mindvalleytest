//
//  Date+.swift
//   Mindvalley-Test
//
//  Created by Mostafizur Rahman on 29/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

extension Date {
    var millisecondsSince1970:Double {
        return Double((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    init(milliseconds:Double) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}

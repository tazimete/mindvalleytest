//
//  ChannelPresenterTest.swift
//  MindvalleyTestTests
//
//  Created by Mostafizur Rahman on 28/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit
import XCTest
@testable import MindvalleyTest

class ChannelListPresenterTest: XCTestCase {
    private var channelListPresenter:ChannelListPresenter!

    override func setUp() {
        channelListPresenter = ChannelListPresenter()
        channelListPresenter.view = MockChannelListView()
        channelListPresenter.interactor = MockChannelListInteractor()
        channelListPresenter.interactor?.input = MockChannelListInteractorInput()
        channelListPresenter.interactor?.input?.apiClient = MockApiRequest()
        channelListPresenter.interactor?.input?.ouput = channelListPresenter
    }
    
    override func tearDown() {
        channelListPresenter = nil
    }
    
    func testViewDidLoad(){
        XCTAssertEqual((channelListPresenter.view as? MockChannelListView)?.isViewLoaded ?? false, false)
        
        channelListPresenter.viewDidLoad()
        
        XCTAssertEqual((channelListPresenter.view as? MockChannelListView)?.isViewLoaded ?? false , true)
    }
    
    func testLoadAllChannelData(){
        channelListPresenter.loadAllChannelData()
        
        let newEpisodeListPresenter = channelListPresenter.newEpisodeList
        let channelCollectionListPresenter = channelListPresenter.channelList
        let categoryListPresenter = channelListPresenter.categoryList
        
        let newEpisodeListInteractor = channelListPresenter.interactor?.input?.newEpisodeList
        let channelCollectionListtInteractor = channelListPresenter.interactor?.input?.channelList
        let categoryListInteractor = channelListPresenter.interactor?.input?.categoryList

        //test new episode
        XCTAssertEqual(newEpisodeListPresenter?[0].title , newEpisodeListInteractor?[0].title)
        XCTAssertEqual(newEpisodeListPresenter?[0].channelName, newEpisodeListInteractor?[0].channelName)
        XCTAssertEqual(newEpisodeListPresenter?[0].coverPicture, newEpisodeListInteractor?[0].coverPicture)
        XCTAssertEqual(newEpisodeListPresenter?[0].type, newEpisodeListInteractor?[0].type)

        XCTAssertNotEqual(newEpisodeListPresenter?[0].title, newEpisodeListInteractor?[1].title)
        XCTAssertNotEqual(newEpisodeListPresenter?[0].channelName, newEpisodeListInteractor?[1].channelName)
        XCTAssertNotEqual(newEpisodeListPresenter?[0].coverPicture, newEpisodeListInteractor?[1].coverPicture)

        XCTAssertEqual(newEpisodeListPresenter?[1].title, newEpisodeListInteractor?[1].title)
        XCTAssertEqual(newEpisodeListPresenter?[1].channelName, newEpisodeListInteractor?[1].channelName)
        XCTAssertEqual(newEpisodeListPresenter?[1].coverPicture, newEpisodeListInteractor?[1].coverPicture)
        XCTAssertEqual(newEpisodeListPresenter?[1].type, newEpisodeListInteractor?[1].type)

        XCTAssertNotEqual(newEpisodeListPresenter?[1].title, newEpisodeListInteractor?[0].title)
        XCTAssertNotEqual(newEpisodeListPresenter?[1].channelName, newEpisodeListInteractor?[0].channelName)
        XCTAssertNotEqual(newEpisodeListPresenter?[1].coverPicture, newEpisodeListInteractor?[0].coverPicture)


        //test channel
        XCTAssertEqual(channelCollectionListPresenter?[0].id, channelCollectionListtInteractor?[0].id)
        XCTAssertEqual(channelCollectionListPresenter?[0].title, channelCollectionListtInteractor?[0].title)
        XCTAssertEqual(channelCollectionListPresenter?[0].iconUrl, channelCollectionListtInteractor?[0].iconUrl)
        XCTAssertEqual(channelCollectionListPresenter?[0].coverUrl, channelCollectionListtInteractor?[0].coverUrl)

        XCTAssertNotEqual(channelCollectionListPresenter?[0].id, channelCollectionListtInteractor?[1].id)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].title, channelCollectionListtInteractor?[1].title)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].iconUrl, channelCollectionListtInteractor?[1].iconUrl)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].coverUrl, channelCollectionListtInteractor?[1].coverUrl)

        XCTAssertEqual(channelCollectionListPresenter?[1].id, channelCollectionListtInteractor?[1].id)
        XCTAssertEqual(channelCollectionListPresenter?[1].title, channelCollectionListtInteractor?[1].title)
        XCTAssertEqual(channelCollectionListPresenter?[1].iconUrl, channelCollectionListtInteractor?[1].iconUrl)
        XCTAssertEqual(channelCollectionListPresenter?[1].coverUrl, channelCollectionListtInteractor?[1].coverUrl)

        XCTAssertNotEqual(channelCollectionListPresenter?[1].id, channelCollectionListtInteractor?[0].id)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].title, channelCollectionListtInteractor?[0].title)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].iconUrl, channelCollectionListtInteractor?[0].iconUrl)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].coverUrl, channelCollectionListtInteractor?[0].coverUrl)

        XCTAssertEqual(channelCollectionListPresenter?[0].seriesList?.count, channelCollectionListtInteractor?[0].seriesList?.count)
        XCTAssertEqual(channelCollectionListPresenter?[0].courseList?.count, channelCollectionListtInteractor?[0].courseList?.count)
        XCTAssertEqual(channelCollectionListPresenter?[1].seriesList?.count, channelCollectionListtInteractor?[1].seriesList?.count)
        XCTAssertEqual(channelCollectionListPresenter? [1].courseList?.count, channelCollectionListtInteractor?[1].courseList?.count)
        
        XCTAssertEqual(channelCollectionListPresenter?[0].seriesList, channelCollectionListtInteractor?[0].seriesList)
        XCTAssertEqual(channelCollectionListPresenter?[0].courseList, channelCollectionListtInteractor?[0].courseList)
        XCTAssertEqual(channelCollectionListPresenter?[1].seriesList, channelCollectionListtInteractor?[1].seriesList)
        XCTAssertEqual(channelCollectionListPresenter?[1].courseList, channelCollectionListtInteractor?[1].courseList)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].seriesList, channelCollectionListtInteractor?[1].seriesList)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].courseList, channelCollectionListtInteractor?[1].courseList)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].seriesList, channelCollectionListtInteractor?[0].seriesList)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].courseList, channelCollectionListtInteractor?[0].courseList)

        //category test
        XCTAssertEqual(categoryListPresenter?.count, categoryListInteractor?.count)
        XCTAssertEqual(categoryListPresenter?[0].name, categoryListInteractor?[0].name)
        XCTAssertEqual(categoryListPresenter?[1].name, categoryListInteractor?[1].name)

        XCTAssertNotEqual(categoryListPresenter?[0].name, categoryListInteractor?[1].name)
        XCTAssertNotEqual(categoryListPresenter?[1].name, categoryListInteractor?[0].name)

        XCTAssertEqual(channelListPresenter.newEpisodeList, channelListPresenter.interactor?.input?.newEpisodeList)
        XCTAssertEqual(channelListPresenter.channelList, channelListPresenter.interactor?.input?.channelList)
        XCTAssertEqual(channelListPresenter.categoryList, channelListPresenter.interactor?.input?.categoryList)
    }
    
    func testLoadNewEpisodeData(){
        channelListPresenter.loadNewEpisodeData()
        
        let newEpisodeListPresenter = channelListPresenter.newEpisodeList
        let newEpisodeListInteractor = channelListPresenter.interactor?.input?.newEpisodeList
        
        //test new episode
        XCTAssertEqual(newEpisodeListPresenter?[0].title , newEpisodeListInteractor?[0].title)
        XCTAssertEqual(newEpisodeListPresenter?[0].channelName, newEpisodeListInteractor?[0].channelName)
        XCTAssertEqual(newEpisodeListPresenter?[0].coverPicture, newEpisodeListInteractor?[0].coverPicture)
        XCTAssertEqual(newEpisodeListPresenter?[0].type, newEpisodeListInteractor?[0].type)

        XCTAssertNotEqual(newEpisodeListPresenter?[0].title, newEpisodeListInteractor?[1].title)
        XCTAssertNotEqual(newEpisodeListPresenter?[0].channelName, newEpisodeListInteractor?[1].channelName)
        XCTAssertNotEqual(newEpisodeListPresenter?[0].coverPicture, newEpisodeListInteractor?[1].coverPicture)

        XCTAssertEqual(newEpisodeListPresenter?[1].title, newEpisodeListInteractor?[1].title)
        XCTAssertEqual(newEpisodeListPresenter?[1].channelName, newEpisodeListInteractor?[1].channelName)
        XCTAssertEqual(newEpisodeListPresenter?[1].coverPicture, newEpisodeListInteractor?[1].coverPicture)
        XCTAssertEqual(newEpisodeListPresenter?[1].type, newEpisodeListInteractor?[1].type)

        XCTAssertNotEqual(newEpisodeListPresenter?[1].title, newEpisodeListInteractor?[0].title)
        XCTAssertNotEqual(newEpisodeListPresenter?[1].channelName, newEpisodeListInteractor?[0].channelName)
        XCTAssertNotEqual(newEpisodeListPresenter?[1].coverPicture, newEpisodeListInteractor?[0].coverPicture)
    }
    
    func testLoadChannelData(){
        channelListPresenter.loadChannelData()
        
        let channelCollectionListPresenter = channelListPresenter.channelList
        let channelCollectionListtInteractor = channelListPresenter.interactor?.input?.channelList
        
        //test channel
        XCTAssertEqual(channelCollectionListPresenter?[0].id, channelCollectionListtInteractor?[0].id)
        XCTAssertEqual(channelCollectionListPresenter?[0].title, channelCollectionListtInteractor?[0].title)
        XCTAssertEqual(channelCollectionListPresenter?[0].iconUrl, channelCollectionListtInteractor?[0].iconUrl)
        XCTAssertEqual(channelCollectionListPresenter?[0].coverUrl, channelCollectionListtInteractor?[0].coverUrl)

        XCTAssertNotEqual(channelCollectionListPresenter?[0].id, channelCollectionListtInteractor?[1].id)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].title, channelCollectionListtInteractor?[1].title)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].iconUrl, channelCollectionListtInteractor?[1].iconUrl)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].coverUrl, channelCollectionListtInteractor?[1].coverUrl)

        XCTAssertEqual(channelCollectionListPresenter?[1].id, channelCollectionListtInteractor?[1].id)
        XCTAssertEqual(channelCollectionListPresenter?[1].title, channelCollectionListtInteractor?[1].title)
        XCTAssertEqual(channelCollectionListPresenter?[1].iconUrl, channelCollectionListtInteractor?[1].iconUrl)
        XCTAssertEqual(channelCollectionListPresenter?[1].coverUrl, channelCollectionListtInteractor?[1].coverUrl)

        XCTAssertNotEqual(channelCollectionListPresenter?[1].id, channelCollectionListtInteractor?[0].id)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].title, channelCollectionListtInteractor?[0].title)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].iconUrl, channelCollectionListtInteractor?[0].iconUrl)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].coverUrl, channelCollectionListtInteractor?[0].coverUrl)

        XCTAssertEqual(channelCollectionListPresenter?[0].seriesList?.count, channelCollectionListtInteractor?[0].seriesList?.count)
        XCTAssertEqual(channelCollectionListPresenter?[0].courseList?.count, channelCollectionListtInteractor?[0].courseList?.count)
        XCTAssertEqual(channelCollectionListPresenter?[1].seriesList?.count, channelCollectionListtInteractor?[1].seriesList?.count)
        XCTAssertEqual(channelCollectionListPresenter? [1].courseList?.count, channelCollectionListtInteractor?[1].courseList?.count)
        
        XCTAssertEqual(channelCollectionListPresenter?[0].seriesList, channelCollectionListtInteractor?[0].seriesList)
        XCTAssertEqual(channelCollectionListPresenter?[0].courseList, channelCollectionListtInteractor?[0].courseList)
        XCTAssertEqual(channelCollectionListPresenter?[1].seriesList, channelCollectionListtInteractor?[1].seriesList)
        XCTAssertEqual(channelCollectionListPresenter?[1].courseList, channelCollectionListtInteractor?[1].courseList)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].seriesList, channelCollectionListtInteractor?[1].seriesList)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].courseList, channelCollectionListtInteractor?[1].courseList)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].seriesList, channelCollectionListtInteractor?[0].seriesList)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].courseList, channelCollectionListtInteractor?[0].courseList)
    }
    
    func testLoadCategoryData(){
        channelListPresenter.loadCategoryData()
        
        let categoryListPresenter = channelListPresenter.categoryList
        let categoryListInteractor = channelListPresenter.interactor?.input?.categoryList
        
        //category test
        XCTAssertEqual(categoryListPresenter?.count, categoryListInteractor?.count)
        XCTAssertEqual(categoryListPresenter?[0].name, categoryListInteractor?[0].name)
        XCTAssertEqual(categoryListPresenter?[1].name, categoryListInteractor?[1].name)

        XCTAssertNotEqual(categoryListPresenter?[0].name, categoryListInteractor?[1].name)
        XCTAssertNotEqual(categoryListPresenter?[1].name, categoryListInteractor?[0].name)

        XCTAssertEqual(channelListPresenter.newEpisodeList, channelListPresenter.interactor?.input?.newEpisodeList)
        XCTAssertEqual(channelListPresenter.channelList, channelListPresenter.interactor?.input?.channelList)
        XCTAssertEqual(channelListPresenter.categoryList, channelListPresenter.interactor?.input?.categoryList)
    }
    
    func testResetAllChannelData(){
        channelListPresenter.resetAllChannelData()
        
        XCTAssertEqual(channelListPresenter.newEpisodeList?.count ?? 0, 0)
        XCTAssertEqual(channelListPresenter.channelList?.count ?? 0, 0)
        XCTAssertEqual(channelListPresenter.categoryList?.count ?? 0, 0)
        
        XCTAssertEqual(channelListPresenter.channelList?[0].seriesList?.count, channelListPresenter.interactor?.input?.channelList?[0].seriesList?.count)
        XCTAssertEqual(channelListPresenter.channelList?[0].courseList?.count, channelListPresenter.interactor?.input?.channelList?[0].courseList?.count)
        XCTAssertEqual(channelListPresenter.channelList?[1].seriesList?.count, channelListPresenter.interactor?.input?.channelList?[1].seriesList?.count)
        XCTAssertEqual(channelListPresenter.channelList? [1].courseList?.count, channelListPresenter.interactor?.input?.channelList? [1].courseList?.count)
    }
    
    func testDidNewEpisodeDataLoaded(){
        channelListPresenter.loadNewEpisodeData()
        channelListPresenter.didNewEpisodeDataLoaded(newEpisodeList: channelListPresenter.interactor?.input?.newEpisodeList ?? [NewEpisode]())
        
        let newEpisodeListPresenter = channelListPresenter.newEpisodeList
        let newEpisodeListInteractor = channelListPresenter.interactor?.input?.newEpisodeList
        
        //test new episode
        XCTAssertEqual(newEpisodeListPresenter?[0].title , newEpisodeListInteractor?[0].title)
        XCTAssertEqual(newEpisodeListPresenter?[0].channelName, newEpisodeListInteractor?[0].channelName)
        XCTAssertEqual(newEpisodeListPresenter?[0].coverPicture, newEpisodeListInteractor?[0].coverPicture)
        XCTAssertEqual(newEpisodeListPresenter?[0].type, newEpisodeListInteractor?[0].type)

        XCTAssertNotEqual(newEpisodeListPresenter?[0].title, newEpisodeListInteractor?[1].title)
        XCTAssertNotEqual(newEpisodeListPresenter?[0].channelName, newEpisodeListInteractor?[1].channelName)
        XCTAssertNotEqual(newEpisodeListPresenter?[0].coverPicture, newEpisodeListInteractor?[1].coverPicture)

        XCTAssertEqual(newEpisodeListPresenter?[1].title, newEpisodeListInteractor?[1].title)
        XCTAssertEqual(newEpisodeListPresenter?[1].channelName, newEpisodeListInteractor?[1].channelName)
        XCTAssertEqual(newEpisodeListPresenter?[1].coverPicture, newEpisodeListInteractor?[1].coverPicture)
        XCTAssertEqual(newEpisodeListPresenter?[1].type, newEpisodeListInteractor?[1].type)

        XCTAssertNotEqual(newEpisodeListPresenter?[1].title, newEpisodeListInteractor?[0].title)
        XCTAssertNotEqual(newEpisodeListPresenter?[1].channelName, newEpisodeListInteractor?[0].channelName)
        XCTAssertNotEqual(newEpisodeListPresenter?[1].coverPicture, newEpisodeListInteractor?[0].coverPicture)
    }
    
    func testDidChannelDataLoaded(){
        channelListPresenter.loadChannelData()
        channelListPresenter.didChannelDataLoaded(channelList: channelListPresenter.interactor?.input?.channelList ?? [Channel]())
        
        let channelCollectionListPresenter = channelListPresenter.channelList
        let channelCollectionListtInteractor = channelListPresenter.interactor?.input?.channelList
        
        //test channel
        XCTAssertEqual(channelCollectionListPresenter?[0].id, channelCollectionListtInteractor?[0].id)
        XCTAssertEqual(channelCollectionListPresenter?[0].title, channelCollectionListtInteractor?[0].title)
        XCTAssertEqual(channelCollectionListPresenter?[0].iconUrl, channelCollectionListtInteractor?[0].iconUrl)
        XCTAssertEqual(channelCollectionListPresenter?[0].coverUrl, channelCollectionListtInteractor?[0].coverUrl)

        XCTAssertNotEqual(channelCollectionListPresenter?[0].id, channelCollectionListtInteractor?[1].id)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].title, channelCollectionListtInteractor?[1].title)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].iconUrl, channelCollectionListtInteractor?[1].iconUrl)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].coverUrl, channelCollectionListtInteractor?[1].coverUrl)

        XCTAssertEqual(channelCollectionListPresenter?[1].id, channelCollectionListtInteractor?[1].id)
        XCTAssertEqual(channelCollectionListPresenter?[1].title, channelCollectionListtInteractor?[1].title)
        XCTAssertEqual(channelCollectionListPresenter?[1].iconUrl, channelCollectionListtInteractor?[1].iconUrl)
        XCTAssertEqual(channelCollectionListPresenter?[1].coverUrl, channelCollectionListtInteractor?[1].coverUrl)

        XCTAssertNotEqual(channelCollectionListPresenter?[1].id, channelCollectionListtInteractor?[0].id)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].title, channelCollectionListtInteractor?[0].title)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].iconUrl, channelCollectionListtInteractor?[0].iconUrl)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].coverUrl, channelCollectionListtInteractor?[0].coverUrl)

        XCTAssertEqual(channelCollectionListPresenter?[0].seriesList?.count, channelCollectionListtInteractor?[0].seriesList?.count)
        XCTAssertEqual(channelCollectionListPresenter?[0].courseList?.count, channelCollectionListtInteractor?[0].courseList?.count)
        XCTAssertEqual(channelCollectionListPresenter?[1].seriesList?.count, channelCollectionListtInteractor?[1].seriesList?.count)
        XCTAssertEqual(channelCollectionListPresenter? [1].courseList?.count, channelCollectionListtInteractor?[1].courseList?.count)
        
        XCTAssertEqual(channelCollectionListPresenter?[0].seriesList, channelCollectionListtInteractor?[0].seriesList)
        XCTAssertEqual(channelCollectionListPresenter?[0].courseList, channelCollectionListtInteractor?[0].courseList)
        XCTAssertEqual(channelCollectionListPresenter?[1].seriesList, channelCollectionListtInteractor?[1].seriesList)
        XCTAssertEqual(channelCollectionListPresenter?[1].courseList, channelCollectionListtInteractor?[1].courseList)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].seriesList, channelCollectionListtInteractor?[1].seriesList)
        XCTAssertNotEqual(channelCollectionListPresenter?[0].courseList, channelCollectionListtInteractor?[1].courseList)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].seriesList, channelCollectionListtInteractor?[0].seriesList)
        XCTAssertNotEqual(channelCollectionListPresenter?[1].courseList, channelCollectionListtInteractor?[0].courseList)
    }
    
    func testDidCategoryDataLoaded(){
        channelListPresenter.loadCategoryData()
        channelListPresenter.didCategoryDataLoaded(categoryList: channelListPresenter.interactor?.input?.categoryList ?? [MindvalleyTest.Category]())
        
        let categoryListPresenter = channelListPresenter.categoryList
        let categoryListInteractor = channelListPresenter.interactor?.input?.categoryList
        
        //category test
        XCTAssertEqual(categoryListPresenter?.count, categoryListInteractor?.count)
        XCTAssertEqual(categoryListPresenter?[0].name, categoryListInteractor?[0].name)
        XCTAssertEqual(categoryListPresenter?[1].name, categoryListInteractor?[1].name)

        XCTAssertNotEqual(categoryListPresenter?[0].name, categoryListInteractor?[1].name)
        XCTAssertNotEqual(categoryListPresenter?[1].name, categoryListInteractor?[0].name)

        XCTAssertEqual(channelListPresenter.newEpisodeList, channelListPresenter.interactor?.input?.newEpisodeList)
        XCTAssertEqual(channelListPresenter.channelList, channelListPresenter.interactor?.input?.channelList)
        XCTAssertEqual(channelListPresenter.categoryList, channelListPresenter.interactor?.input?.categoryList)
    }
    
    
    func testGetNewEpisodeAtPosition(){
        channelListPresenter.loadNewEpisodeData()
        let newEpisode = channelListPresenter.getNewEpisode(at: 0)
        
        XCTAssertEqual(channelListPresenter.newEpisodeList?[0].title , newEpisode?.title)
        XCTAssertEqual(channelListPresenter.newEpisodeList?[0].channelName, newEpisode?.channelName)
        XCTAssertEqual(channelListPresenter.newEpisodeList?[0].coverPicture, newEpisode?.coverPicture)
        XCTAssertEqual(channelListPresenter.interactor?.input?.newEpisodeList?[0].type, newEpisode?.type)
    }
    
    func testGetChannelAtPosition(){
        channelListPresenter.loadChannelData()
        let channel = channelListPresenter.getChannel(at: 0)
        
        XCTAssertEqual(channelListPresenter.channelList?[0].id, channel?.id)
        XCTAssertEqual(channelListPresenter.channelList?[0].title, channel?.title)
        XCTAssertEqual(channelListPresenter.channelList?[0].iconUrl, channel?.iconUrl)
        XCTAssertEqual(channelListPresenter.channelList?[0].coverUrl, channel?.coverUrl)
        XCTAssertEqual(channelListPresenter.channelList?[0].seriesList?.count, channel?.seriesList?.count)
        XCTAssertEqual(channelListPresenter.channelList?[0].courseList?.count, channel?.courseList?.count)
    }
    
    func testGetCategoryAtPosition(){
        channelListPresenter.loadChannelData()
        let category = channelListPresenter.getCategory(at: 0)
        
        XCTAssertEqual(channelListPresenter.categoryList?[0].name, category?.name)
    }
}


//MARK: MockChannelListPresenter
class MockChannelListPresenter: ChannelListPresenterProtocol, ChannelListInteractorOutputProtocol {
    var newEpisodeList: [NewEpisode]?
    
    var channelList: [Channel]?
    
    var categoryList: [MindvalleyTest.Category]?
    
    var interactor: ChannelListInteractorProtocol?
    
    var view: ChannelListViewProtocol?
    
    var router: ChannelListRouterProtocol?
    
    func viewDidLoad() {
        
    }
    
    func loadAllChannelData() {
        interactor?.loadAllChannelData()
    }
    
    func loadNewEpisodeData() {
        interactor?.loadNewEpisodeData()
    }
    
    func loadChannelData() {
        interactor?.loadChannelData()
    }
    
    func loadCategoryData() {
        interactor?.loadCategoryData()
    }
    
    func getNewEpisode(at position: Int) -> NewEpisode? {
        return newEpisodeList?[position]
    }
    
    func getChannel(at position: Int) -> Channel? {
        return channelList?[position]
    }
    
    func getCategory(at position: Int) -> MindvalleyTest.Category? {
        return categoryList?[position]
    }
    
    func didNewEpisodeDataLoaded(newEpisodeList: [NewEpisode]) {
        self.newEpisodeList = Optional(newEpisodeList)
        view?.didNewEpisodeDataLoaded(newEpisodeList: newEpisodeList)
    }
    
    func didChannelDataLoaded(channelList: [Channel]) {
        self.channelList = Optional(channelList)
        view?.didChannelDataLoaded(channelList: channelList)
    }
    
    func didCategoryDataLoaded(categoryList: [MindvalleyTest.Category]) {
        self.categoryList = Optional(categoryList)
        view?.didCategoryDataLoaded(categoryList: categoryList)
    }
    
    
    func resetAllChannelData() {
        newEpisodeList?.removeAll()
        channelList?.removeAll()
        categoryList?.removeAll()
        
        interactor?.resetAllChannelData()
    }
}


//MARK: MockChannelListView
class MockChannelListView: ChannelListViewProtocol {
    public var presenter:ChannelListPresenterProtocol?
    public var isViewLoaded = false
    
    func initialize() {
        isViewLoaded = true
    }
    
    func didNewEpisodeDataLoaded(newEpisodeList: [NewEpisode]) {
        
    }
    
    func didChannelDataLoaded(channelList: [Channel]) {
        
    }
    
    func didCategoryDataLoaded(categoryList: [MindvalleyTest.Category]) {
        
    }
    
    func didResetAllChannelData() {
        
    }
}

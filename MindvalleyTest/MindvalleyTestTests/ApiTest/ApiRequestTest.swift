//
//  ApiRequestTest.swift
//  MindvalleyTestTests
//
//  Created by Mostafizur Rahman on 29/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit
import XCTest
@testable import MindvalleyTest

class ApiRequestTest: XCTestCase {
    private var newEpisodeList: [NewEpisode]!
    private var channelList: [Channel]!
    private var categoryList: [MindvalleyTest.Category]!
    private var apiClient: MockApiRequest!
    
    
    override func setUp() {
        apiClient = MockApiRequest()
        newEpisodeList = apiClient.getNewEpisodeData()
        channelList = apiClient.getChannelData()
        categoryList = apiClient.getCategoryData()
    }
    
    override func tearDown() {
        newEpisodeList = nil
        channelList = nil
        categoryList = nil
        apiClient = nil
    }
    
    func testGet(){
        apiClient.get(url: "www.test.com/testapi", param: "page=10", completeionHandler: {
            response in
            
            XCTAssertEqual(response?.count ?? 0, 2)
            XCTAssertEqual((response?.value(forKey: "url") as? String) ?? "", "www.test.com/testapi")
            XCTAssertEqual((response?.value(forKey: "page") as? String) ?? "", "10")
        })
    }
    
    func testApiClient() {
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        //test data length
        XCTAssertEqual(apiClient.getChannelData().count, 2)
        XCTAssertNotEqual(apiClient.getChannelData().count, 0)
        
        XCTAssertEqual(apiClient.getNewEpisodeData().count, 2)
        XCTAssertNotEqual(apiClient.getNewEpisodeData().count, 0)
        
        XCTAssertEqual(apiClient.getCategoryData().count, 2)
        XCTAssertNotEqual(apiClient.getCategoryData().count, 0)
        
        //test new episode
        XCTAssertEqual(newEpisodeList[0].title ?? "", "Conscious Parenting")
        XCTAssertEqual(newEpisodeList[0].channelName ?? "", "Little Humans")
        XCTAssertEqual(newEpisodeList[0].coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/5bdbdd0e-3bd3-432b-b8cb-3d3556c58c94.jpg?transform=w_1080")
        XCTAssertEqual(newEpisodeList[0].type ?? "", "course")
        
        XCTAssertNotEqual(newEpisodeList[0].title ?? "", "Recorded Live Calls: Intellectual Life")
        XCTAssertNotEqual(newEpisodeList[0].channelName ?? "", "Lifebook Membership")
        XCTAssertNotEqual(newEpisodeList[0].coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/273a5e20-8088-4e94-8f34-6b0241e93962.jpg?transform=w_1080")
        XCTAssertNotEqual(newEpisodeList[0].type ?? "", "series")
        
        XCTAssertEqual(newEpisodeList[1].title ?? "", "Recorded Live Calls: Intellectual Life")
        XCTAssertEqual(newEpisodeList[1].channelName ?? "", "Lifebook Membership")
        XCTAssertEqual(newEpisodeList[1].coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/273a5e20-8088-4e94-8f34-6b0241e93962.jpg?transform=w_1080")
        XCTAssertEqual(newEpisodeList[1].type ?? "", "course")
        
        XCTAssertNotEqual(newEpisodeList[1].title ?? "", "Conscious Parenting")
        XCTAssertNotEqual(newEpisodeList[1].channelName ?? "", "Little Humans")
        XCTAssertNotEqual(newEpisodeList[1].coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/5bdbdd0e-3bd3-432b-b8cb-3d3556c58c94.jpg?transform=w_1080")
        XCTAssertNotEqual(newEpisodeList[1].type ?? "", "series")
        
        
        //test channel
        XCTAssertEqual(channelList[0].id ?? "", "1")
        XCTAssertEqual(channelList[0].title ?? "", "Mindvalley Films")
        XCTAssertEqual(channelList[0].iconUrl ?? "", "https://assets.mindvalley.com/api/v1/assets/eb493421-c048-492b-a471-bed7604e95d6.jpg?transform=w_1080")
        XCTAssertEqual(channelList[0].coverUrl ?? "", "https://assets.mindvalley.com/api/v1/assets/e819d374-a6d9-4308-bde1-1e145426e546.jpg?transform=w_1080")

        XCTAssertNotEqual(channelList[0].id ?? "", "2")
        XCTAssertNotEqual(channelList[0].title ?? "", "Unlimited Abundance")
        XCTAssertNotEqual(channelList[0].iconUrl ?? "", "")
        XCTAssertNotEqual(channelList[0].coverUrl ?? "", "https://assets.mindvalley.com/api/v1/assets/aa833424-e1b7-463b-b1a1-f7faa222f8d0.jpg?transform=w_1080")
        
        XCTAssertEqual(channelList[1].id ?? "", "2")
        XCTAssertEqual(channelList[1].title ?? "", "Unlimited Abundance")
        XCTAssertEqual(channelList[1].iconUrl ?? "", "")
        XCTAssertEqual(channelList[1].coverUrl ?? "", "https://assets.mindvalley.com/api/v1/assets/aa833424-e1b7-463b-b1a1-f7faa222f8d0.jpg?transform=w_1080")

        XCTAssertNotEqual(channelList[1].id ?? "", "1")
        XCTAssertNotEqual(channelList[1].title ?? "", "Mindvalley Films")
        XCTAssertNotEqual(channelList[1].iconUrl ?? "", "https://assets.mindvalley.com/api/v1/assets/eb493421-c048-492b-a471-bed7604e95d6.jpg?transform=w_1080")
        XCTAssertNotEqual(channelList[1].coverUrl ?? "", "https://assets.mindvalley.com/api/v1/assets/e819d374-a6d9-4308-bde1-1e145426e546.jpg?transform=w_1080")
        
        XCTAssertEqual(channelList[0].seriesList?.count ?? 0, 2)
        XCTAssertEqual(channelList[0].courseList?.count ?? 0, 2)
        XCTAssertEqual(channelList[1].seriesList?.count ?? 0, 2)
        XCTAssertEqual(channelList[1].courseList?.count ?? 0, 2)
        
        //category test
        XCTAssertEqual(categoryList.count, 2)
        XCTAssertEqual(categoryList[0].name, "Character")
        XCTAssertEqual(categoryList[1].name,"Career")

        XCTAssertNotEqual(categoryList.count, 0)
        XCTAssertNotEqual(categoryList[0].name, "")
        XCTAssertNotEqual(categoryList[1].name, "")
    }
    
    
    func testApiRequestGetChannelData(){
        XCTAssertEqual(channelList.count, apiClient.getChannelData().count)
        XCTAssertNotEqual(apiClient.getChannelData().count, 0)
        XCTAssertEqual(channelList, apiClient.getChannelData())
    }

    func testApiRequestGetNewEpisodeData(){
        XCTAssertEqual(newEpisodeList.count, apiClient.getNewEpisodeData().count)
        XCTAssertNotEqual(apiClient.getNewEpisodeData().count, 0)
        XCTAssertEqual(newEpisodeList, apiClient.getNewEpisodeData())
    }
    
    func testApiRequestGetCategoryData(){
        XCTAssertEqual(categoryList.count, apiClient.getCategoryData().count)
        XCTAssertNotEqual(apiClient.getCategoryData().count, 0)
        XCTAssertEqual(categoryList, apiClient.getCategoryData())
    }
}


//MARK: MockApiRequest
class MockApiRequest: Requestable {
    func get(url: String, param: String, completeionHandler: @escaping (NSDictionary?) -> Void) {
        let resposne = NSMutableDictionary()
        resposne.setValue(url, forKey: "url")
        resposne.setValue(String(param.split(separator: "=").last ?? ""), forKey: String(param.split(separator: "=").first ?? ""))
        
        completeionHandler(Optional(resposne as NSDictionary))
    }
    
    func getChannelData() -> [Channel] {
        var channelList = [Channel]()
        
        let channel1 = Channel()
        channel1.id = Optional("1")
        channel1.title = Optional("Mindvalley Films")
        channel1.iconUrl = Optional("https://assets.mindvalley.com/api/v1/assets/eb493421-c048-492b-a471-bed7604e95d6.jpg?transform=w_1080")
        channel1.coverUrl = Optional("https://assets.mindvalley.com/api/v1/assets/e819d374-a6d9-4308-bde1-1e145426e546.jpg?transform=w_1080")
        
        let ch1s1 = Series()
        ch1s1.title = Optional("Human Longevity")
        ch1s1.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/a8ef9c6b-cb5c-4551-b57c-baf261e80cc4.jpg?transform=w_1080")
        ch1s1.channelName = Optional("Mindvalley Films")
        
        let ch1s2 = Series()
        ch1s2.title = Optional("Live Your Quest")
        ch1s2.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/e019134c-fdb0-497c-a613-6b9f1c7166a4.jpg?transform=w_1080")
        ch1s2.channelName = Optional("Mindvalley Films")
        
        channel1.seriesList = Optional([ch1s1, ch1s2])
        
        let ch1c1 = Course()
        ch1c1.title = Optional("The Barcelona Experiment")
        ch1c1.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/e0f1bba7-2292-44e6-abbc-cf0137706181.jpg?transform=w_1080")
        ch1c1.channelName = Optional("Mindvalley Films")
        
        let ch1c2 = Course()
        ch1c2.title = Optional("Live Your Quest")
        ch1c2.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/e019134c-fdb0-497c-a613-6b9f1c7166a4.jpg?transform=w_1080")
        ch1c2.channelName = Optional("Mindvalley Films")
        
        channel1.courseList = Optional([ch1c1, ch1c2])
        
        channelList.append(channel1)
        
        let channel2 = Channel()
        channel2.id = Optional("2")
        channel2.title = Optional("Unlimited Abundance")
        channel2.iconUrl = Optional("")
        channel2.coverUrl = Optional("https://assets.mindvalley.com/api/v1/assets/aa833424-e1b7-463b-b1a1-f7faa222f8d0.jpg?transform=w_1080")
        
        let ch2s1 = Series()
        ch2s1.title = Optional("Unlimited Abundance Program")
        ch2s1.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/34b459cd-5a5c-400e-9e02-ae1481a1a87b.jpg?transform=w_1080")
        ch2s1.channelName = Optional("Unlimited Abundance")
        
        let ch2s2 = Series()
        ch2s2.title = Optional("Bonuses for Unlimited Abundance")
        ch2s2.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/4bdbc2ce-d9cd-47cc-8891-4252a8bf0afc.jpg?transform=w_1080")
        ch2s2.channelName = Optional("Unlimited Abundance")
        
        channel2.seriesList = Optional([ch2s1, ch2s2])
        
        let ch2c1 = Course()
        ch2c1.title = Optional("Introduction Video")
        ch2c1.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/d3225076-45b6-4ad6-b0a7-2316b26e6676.jpg?transform=w_1080")
        ch2c1.channelName = Optional("Unlimited Abundance")
        
        let ch2c2 = Course()
        ch2c2.title = Optional("1. Clearing Resistance")
        ch2c2.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/a6be0a22-6538-466c-80d8-bfcb6ce94b81.jpg?transform=w_1080")
        ch2c2.channelName = Optional("Unlimited Abundance")
        
        channel2.courseList = Optional([ch2c1, ch2c2])
        
        channelList.append(channel2)
        
        return channelList
    }
    
    func getNewEpisodeData() -> [NewEpisode] {
        var newEpisodeList = [NewEpisode]()
        
        let newEpisode1 = NewEpisode()
        newEpisode1.title = Optional("Conscious Parenting")
        newEpisode1.channelName = Optional("Little Humans")
        newEpisode1.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/5bdbdd0e-3bd3-432b-b8cb-3d3556c58c94.jpg?transform=w_1080")
        newEpisode1.type = Optional("course")
        
        newEpisodeList.append(newEpisode1)
        
        let newEpisode2 = NewEpisode()
        newEpisode2.title = Optional("Recorded Live Calls: Intellectual Life")
        newEpisode2.channelName = Optional("Lifebook Membership")
        newEpisode2.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/273a5e20-8088-4e94-8f34-6b0241e93962.jpg?transform=w_1080")
        newEpisode2.type = Optional("course")
        
        newEpisodeList.append(newEpisode2)
        
        
        return newEpisodeList
    }
    
    func getCategoryData() -> [MindvalleyTest.Category] {
        var categoryList = [MindvalleyTest.Category]()
        
        let category1 = Category()
        category1.name = Optional("Character")
        categoryList.append(category1)
        
        let category2 = Category()
        category2.name = Optional("Career")
        categoryList.append(category2)
        return categoryList
    }
}

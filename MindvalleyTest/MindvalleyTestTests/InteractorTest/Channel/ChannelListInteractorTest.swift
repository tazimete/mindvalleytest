//
//  ChannelListInteractor.swift
//  MindvalleyTestTests
//
//  Created by Mostafizur Rahman on 28/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit
import XCTest
@testable import MindvalleyTest

class ChannelListInteractorTest: XCTestCase {
    private var newEpisodeList: [NewEpisode]!
    private var channelList: [Channel]!
    private var categoryList: [MindvalleyTest.Category]!
    private var apiClient: MockApiRequest!
    private var channelListInteractor:ChannelListInteractor!

    override func setUp() {
        apiClient = MockApiRequest()
        newEpisodeList = apiClient.getNewEpisodeData()
        channelList = apiClient.getChannelData()
        categoryList = apiClient.getCategoryData()
        channelListInteractor = ChannelListInteractor()
        channelListInteractor.input = MockChannelListInteractorInput()
        channelListInteractor.input?.apiClient = apiClient
        channelListInteractor.input?.ouput = MockChannelListPresenter()
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        newEpisodeList = nil
        channelList = nil
        categoryList = nil
        apiClient = nil
        channelListInteractor = nil
    }
    
    
    //MARK: Interactor Test
    func testLoadAllChannelData(){
        channelListInteractor.loadAllChannelData()
        
        let newEpisodeListInteractor = channelListInteractor.input?.newEpisodeList
        let channelCollectionListInteractor = channelListInteractor.input?.channelList
        let categoryListInteractor = channelListInteractor.input?.categoryList
        
        let newEpisodeListApiClient = apiClient.getNewEpisodeData()
        let channelCollectionListApiClient = apiClient.getChannelData()
        let categoryListApiClient = apiClient.getCategoryData()
        
        
        //test new episode
        XCTAssertEqual(newEpisodeListInteractor?[0].title ?? "", newEpisodeListApiClient[0].title)
        XCTAssertEqual(newEpisodeListInteractor?[0].channelName ?? "", newEpisodeListApiClient[0].channelName)
        XCTAssertEqual(newEpisodeListInteractor?[0].coverPicture ?? "", newEpisodeListApiClient[0].coverPicture)
        XCTAssertEqual(newEpisodeListInteractor?[0].type ?? "", newEpisodeListApiClient[0].type)

        XCTAssertNotEqual(newEpisodeListInteractor?[0].title ?? "", newEpisodeListApiClient[1].title)
        XCTAssertNotEqual(newEpisodeListInteractor?[0].channelName ?? "", newEpisodeListApiClient[1].channelName)
        XCTAssertNotEqual(newEpisodeListInteractor?[0].coverPicture ?? "", newEpisodeListApiClient[1].coverPicture)

        XCTAssertEqual(newEpisodeListInteractor?[1].title ?? "", newEpisodeListApiClient[1].title)
        XCTAssertEqual(newEpisodeListInteractor?[1].channelName ?? "", newEpisodeListApiClient[1].channelName)
        XCTAssertEqual(newEpisodeListInteractor?[1].coverPicture ?? "", newEpisodeListApiClient[1].coverPicture)
        XCTAssertEqual(newEpisodeListInteractor?[1].type ?? "", newEpisodeListApiClient[1].type)

        XCTAssertNotEqual(newEpisodeListInteractor?[1].title ?? "", newEpisodeListApiClient[0].title)
        XCTAssertNotEqual(newEpisodeListInteractor?[1].channelName ?? "", newEpisodeListApiClient[0].channelName)
        XCTAssertNotEqual(newEpisodeListInteractor?[1].coverPicture ?? "", newEpisodeListApiClient[0].coverPicture)


        //test channel
        XCTAssertEqual(channelCollectionListInteractor?[0].id ?? "", channelCollectionListApiClient[0].id)
        XCTAssertEqual(channelCollectionListInteractor?[0].title ?? "", channelCollectionListApiClient[0].title)
        XCTAssertEqual(channelCollectionListInteractor?[0].iconUrl ?? "", channelCollectionListApiClient[0].iconUrl)
        XCTAssertEqual(channelCollectionListInteractor?[0].coverUrl ?? "", channelCollectionListApiClient[0].coverUrl)

        XCTAssertNotEqual(channelCollectionListInteractor?[0].id ?? "", channelCollectionListApiClient[1].id)
        XCTAssertNotEqual(channelCollectionListInteractor?[0].title ?? "", channelCollectionListApiClient[1].title)
        XCTAssertNotEqual(channelCollectionListInteractor?[0].iconUrl ?? "", channelCollectionListApiClient[1].iconUrl)
        XCTAssertNotEqual(channelCollectionListInteractor?[0].coverUrl ?? "", channelCollectionListApiClient[1].coverUrl)

        XCTAssertEqual(channelCollectionListInteractor?[1].id ?? "", channelCollectionListApiClient[1].id)
        XCTAssertEqual(channelCollectionListInteractor?[1].title ?? "", channelCollectionListApiClient[1].title)
        XCTAssertEqual(channelCollectionListInteractor?[1].iconUrl ?? "", channelCollectionListApiClient[1].iconUrl)
        XCTAssertEqual(channelCollectionListInteractor?[1].coverUrl ?? "", channelCollectionListApiClient[1].coverUrl)

        XCTAssertNotEqual(channelCollectionListInteractor?[1].id ?? "", channelCollectionListApiClient[0].id)
        XCTAssertNotEqual(channelCollectionListInteractor?[1].title ?? "", channelCollectionListApiClient[0].title)
        XCTAssertNotEqual(channelCollectionListInteractor?[1].iconUrl ?? "", channelCollectionListApiClient[0].iconUrl)
        XCTAssertNotEqual(channelCollectionListInteractor?[1].coverUrl ?? "", channelCollectionListApiClient[0].coverUrl)

        XCTAssertEqual(channelCollectionListInteractor?[0].seriesList?.count ?? 0, channelCollectionListApiClient[0].seriesList?.count)
        XCTAssertEqual(channelCollectionListInteractor?[0].courseList?.count ?? 0, channelCollectionListApiClient[0].courseList?.count)
        XCTAssertEqual(channelCollectionListInteractor?[1].seriesList?.count ?? 0, channelCollectionListApiClient[1].seriesList?.count)
        XCTAssertEqual(channelCollectionListInteractor? [1].courseList?.count ?? 0, channelCollectionListApiClient[1].courseList?.count)
        
        XCTAssertEqual(channelCollectionListInteractor?[0].seriesList ?? [Series](), channelCollectionListApiClient[0].seriesList)
        XCTAssertEqual(channelCollectionListInteractor?[0].courseList ?? [Course](), channelCollectionListApiClient[0].courseList)
        XCTAssertEqual(channelCollectionListInteractor?[1].seriesList ?? [Series](), channelCollectionListApiClient[1].seriesList)
        XCTAssertEqual(channelCollectionListInteractor?[1].courseList ?? [Course](), channelCollectionListApiClient[1].courseList)
        XCTAssertNotEqual(channelCollectionListInteractor?[0].seriesList ?? [Series](), channelCollectionListApiClient[1].seriesList)
        XCTAssertNotEqual(channelCollectionListInteractor?[0].courseList ?? [Course](), channelCollectionListApiClient[1].courseList)
        XCTAssertNotEqual(channelCollectionListInteractor?[1].seriesList ?? [Series](), channelCollectionListApiClient[0].seriesList)
        XCTAssertNotEqual(channelCollectionListInteractor?[1].courseList ?? [Course](), channelCollectionListApiClient[0].courseList)

        //category test
        XCTAssertEqual(categoryListInteractor?.count ?? 0, categoryListApiClient.count)
        XCTAssertEqual(categoryListInteractor?[0].name ?? "", categoryListApiClient[0].name)
        XCTAssertEqual(categoryListInteractor?[1].name ?? "", categoryListApiClient[1].name)

        XCTAssertNotEqual(categoryListInteractor?[0].name ?? "", categoryListApiClient[1].name)
        XCTAssertNotEqual(categoryListInteractor?[1].name ?? "", categoryListApiClient[0].name)

        XCTAssertEqual(channelListInteractor.input?.newEpisodeList ?? [NewEpisode](), apiClient.getNewEpisodeData())
        XCTAssertEqual(channelListInteractor.input?.channelList ?? [Channel](), apiClient.getChannelData())
        XCTAssertEqual(channelListInteractor.input?.categoryList ?? [MindvalleyTest.Category](), apiClient.getCategoryData())
    }
    
    func testLoadNewEpisodeData(){
        channelListInteractor.loadNewEpisodeData()
        
        let newEpisodeListInteractor = channelListInteractor.input?.newEpisodeList
        let newEpisodeListApiClient = apiClient.getNewEpisodeData()
        
        XCTAssertEqual(newEpisodeListInteractor?[0].title ?? "", newEpisodeListApiClient[0].title)
        XCTAssertEqual(newEpisodeListInteractor?[0].channelName ?? "", newEpisodeListApiClient[0].channelName)
        XCTAssertEqual(newEpisodeListInteractor?[0].coverPicture ?? "", newEpisodeListApiClient[0].coverPicture)
        XCTAssertEqual(newEpisodeListInteractor?[0].type ?? "", newEpisodeListApiClient[0].type)

        XCTAssertNotEqual(newEpisodeListInteractor?[0].title ?? "", newEpisodeListApiClient[1].title)
        XCTAssertNotEqual(newEpisodeListInteractor?[0].channelName ?? "", newEpisodeListApiClient[1].channelName)
        XCTAssertNotEqual(newEpisodeListInteractor?[0].coverPicture ?? "", newEpisodeListApiClient[1].coverPicture)

        XCTAssertEqual(newEpisodeListInteractor?[1].title ?? "", newEpisodeListApiClient[1].title)
        XCTAssertEqual(newEpisodeListInteractor?[1].channelName ?? "", newEpisodeListApiClient[1].channelName)
        XCTAssertEqual(newEpisodeListInteractor?[1].coverPicture ?? "", newEpisodeListApiClient[1].coverPicture)
        XCTAssertEqual(newEpisodeListInteractor?[1].type ?? "", newEpisodeListApiClient[1].type)

        XCTAssertNotEqual(newEpisodeListInteractor?[1].title ?? "", newEpisodeListApiClient[0].title)
        XCTAssertNotEqual(newEpisodeListInteractor?[1].channelName ?? "", newEpisodeListApiClient[0].channelName)
        XCTAssertNotEqual(newEpisodeListInteractor?[1].coverPicture ?? "", newEpisodeListApiClient[0].coverPicture)
        
        XCTAssertEqual(newEpisodeListInteractor ?? [NewEpisode](), newEpisodeListApiClient)
    }
    
    func testLoadChannelData(){
        channelListInteractor.loadChannelData()
        
        let channelCollectionListInteractor = channelListInteractor.input?.channelList
        let channelCollectionListApiClient = apiClient.getChannelData()
        
        XCTAssertEqual(channelCollectionListInteractor?[0].id ?? "", channelCollectionListApiClient[0].id)
        XCTAssertEqual(channelCollectionListInteractor?[0].title ?? "", channelCollectionListApiClient[0].title)
        XCTAssertEqual(channelCollectionListInteractor?[0].iconUrl ?? "", channelCollectionListApiClient[0].iconUrl)
        XCTAssertEqual(channelCollectionListInteractor?[0].coverUrl ?? "", channelCollectionListApiClient[0].coverUrl)

        XCTAssertNotEqual(channelCollectionListInteractor?[0].id ?? "", channelCollectionListApiClient[1].id)
        XCTAssertNotEqual(channelCollectionListInteractor?[0].title ?? "", channelCollectionListApiClient[1].title)
        XCTAssertNotEqual(channelCollectionListInteractor?[0].iconUrl ?? "", channelCollectionListApiClient[1].iconUrl)
        XCTAssertNotEqual(channelCollectionListInteractor?[0].coverUrl ?? "", channelCollectionListApiClient[1].coverUrl)

        XCTAssertEqual(channelCollectionListInteractor?[1].id ?? "", channelCollectionListApiClient[1].id)
        XCTAssertEqual(channelCollectionListInteractor?[1].title ?? "", channelCollectionListApiClient[1].title)
        XCTAssertEqual(channelCollectionListInteractor?[1].iconUrl ?? "", channelCollectionListApiClient[1].iconUrl)
        XCTAssertEqual(channelCollectionListInteractor?[1].coverUrl ?? "", channelCollectionListApiClient[1].coverUrl)

        XCTAssertNotEqual(channelCollectionListInteractor?[1].id ?? "", channelCollectionListApiClient[0].id)
        XCTAssertNotEqual(channelCollectionListInteractor?[1].title ?? "", channelCollectionListApiClient[0].title)
        XCTAssertNotEqual(channelCollectionListInteractor?[1].iconUrl ?? "", channelCollectionListApiClient[0].iconUrl)
        XCTAssertNotEqual(channelCollectionListInteractor?[1].coverUrl ?? "", channelCollectionListApiClient[0].coverUrl)

        XCTAssertEqual(channelCollectionListInteractor?[0].seriesList?.count ?? 0, channelCollectionListApiClient[0].seriesList?.count)
        XCTAssertEqual(channelCollectionListInteractor?[0].courseList?.count ?? 0, channelCollectionListApiClient[0].courseList?.count)
        XCTAssertEqual(channelCollectionListInteractor?[1].seriesList?.count ?? 0, channelCollectionListApiClient[1].seriesList?.count)
        XCTAssertEqual(channelCollectionListInteractor? [1].courseList?.count ?? 0, channelCollectionListApiClient[1].courseList?.count)
        
        XCTAssertEqual(channelCollectionListInteractor?[0].seriesList ?? [Series](), channelCollectionListApiClient[0].seriesList)
        XCTAssertEqual(channelCollectionListInteractor?[0].courseList ?? [Course](), channelCollectionListApiClient[0].courseList)
        XCTAssertEqual(channelCollectionListInteractor?[1].seriesList ?? [Series](), channelCollectionListApiClient[1].seriesList)
        XCTAssertEqual(channelCollectionListInteractor?[1].courseList ?? [Course](), channelCollectionListApiClient[1].courseList)
        XCTAssertNotEqual(channelCollectionListInteractor?[0].seriesList ?? [Series](), channelCollectionListApiClient[1].seriesList)
        XCTAssertNotEqual(channelCollectionListInteractor?[0].courseList ?? [Course](), channelCollectionListApiClient[1].courseList)
        XCTAssertNotEqual(channelCollectionListInteractor?[1].seriesList ?? [Series](), channelCollectionListApiClient[0].seriesList)
        XCTAssertNotEqual(channelCollectionListInteractor?[1].courseList ?? [Course](), channelCollectionListApiClient[0].courseList)
        
        XCTAssertEqual(channelCollectionListInteractor ?? [Channel](), channelCollectionListApiClient)
    }
    
    func testLoadCategoryData(){
        channelListInteractor.loadCategoryData()
        
        let categoryListInteractor = channelListInteractor.input?.categoryList
        let categoryListApiClient = apiClient.getCategoryData()
        
        //category test
        XCTAssertEqual(categoryListInteractor?.count ?? 0, categoryListApiClient.count)
        XCTAssertEqual(categoryListInteractor?[0].name ?? "", categoryListApiClient[0].name)
        XCTAssertEqual(categoryListInteractor?[1].name ?? "", categoryListApiClient[1].name)

        XCTAssertNotEqual(categoryListInteractor?[0].name ?? "", categoryListApiClient[1].name)
        XCTAssertNotEqual(categoryListInteractor?[1].name ?? "", categoryListApiClient[0].name)

        XCTAssertEqual(categoryListInteractor ?? [MindvalleyTest.Category](), categoryListApiClient)
    }
    
    func testResetAllChannelData(){
        channelListInteractor.resetAllChannelData()
        
        XCTAssertEqual(channelListInteractor.input?.newEpisodeList?.count ?? 0, 0)
        XCTAssertEqual(channelListInteractor.input?.channelList?.count ?? 0, 0)
        XCTAssertEqual(channelListInteractor.input?.categoryList?.count ?? 0, 0)
        
        XCTAssertNotEqual(channelListInteractor.input?.newEpisodeList ?? [NewEpisode](), apiClient.getNewEpisodeData())
        XCTAssertNotEqual(channelListInteractor.input?.channelList ?? [Channel](), apiClient.getChannelData())
        XCTAssertNotEqual(channelListInteractor.input?.categoryList ?? [MindvalleyTest.Category](), apiClient.getCategoryData())
    }
}


//MARK: MockChannelListInteractor
class MockChannelListInteractor : ChannelListInteractorProtocol{
    var input: ChannelListInteractorInputProtocol?
    
    func loadAllChannelData() {
        input?.loadAllChannelData()
    }
    
    func loadNewEpisodeData() {
        input?.loadNewEpisodeData()
    }
    
    func loadChannelData() {
        input?.loadChannelData()
    }
    
    func loadCategoryData() {
        input?.loadCategoryData()
    }
    
    func resetAllChannelData() {
        input?.resetAllChannelData()
    }

}

//MARK: MockChannelListInteractorInput
class MockChannelListInteractorInput : ChannelListInteractorInputProtocol{
    var newEpisodeList: [NewEpisode]?
    var channelList: [Channel]?
    var categoryList: [MindvalleyTest.Category]?
    var ouput: ChannelListInteractorOutputProtocol?
    var apiClient: Requestable?
    
    
    func loadAllChannelData() {
        loadNewEpisodeData()
        loadChannelData()
        loadCategoryData()
    }
    
    func loadNewEpisodeData() {
        newEpisodeList = (apiClient!.getNewEpisodeData())
        ouput?.didNewEpisodeDataLoaded(newEpisodeList: newEpisodeList ?? [NewEpisode]())
    }
    
    func loadChannelData() {
        channelList = (apiClient!.getChannelData())
        ouput?.didChannelDataLoaded(channelList: channelList ?? [Channel]())
    }
    
    func loadCategoryData() {
        categoryList = (apiClient!.getCategoryData())
        ouput?.didCategoryDataLoaded(categoryList: categoryList ?? [MindvalleyTest.Category]())
    }
    
    func resetAllChannelData() {
        newEpisodeList?.removeAll()
        channelList?.removeAll()
        categoryList?.removeAll()
    }
}


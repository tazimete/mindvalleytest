//
//  ChannelListInteractorInputTest.swift
//  MindvalleyTestTests
//
//  Created by Mostafizur Rahman on 29/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit
import XCTest
@testable import MindvalleyTest

class ChannelListInteractorInputTest: XCTestCase {

     private var channelListInteractorInput:ChannelListInteractorInput!
    
    override func setUp() {
        channelListInteractorInput = ChannelListInteractorInput()
        channelListInteractorInput.apiClient = MockApiRequest()
    }
    
    override func tearDown() {
        channelListInteractorInput = nil
    }
    
    func testLoadAllChannelData(){
        channelListInteractorInput.loadAllChannelData()

        //test new episode
        XCTAssertEqual(channelListInteractorInput.newEpisodeList ?? [NewEpisode]() , [NewEpisode]())
        XCTAssertEqual(channelListInteractorInput.newEpisodeList?.count ?? 0 , 0)

        //test channel
        XCTAssertEqual(channelListInteractorInput.channelList ?? [Channel](), [Channel]())
        XCTAssertEqual(channelListInteractorInput.channelList?.count ?? 0 , 0)

        //category test
        XCTAssertEqual(channelListInteractorInput.categoryList?.count ?? 0, 0)
        XCTAssertEqual(channelListInteractorInput.categoryList ?? [MindvalleyTest.Category](), [MindvalleyTest.Category]())
    }
    
    func testLoadNewEpisodeData(){
        channelListInteractorInput.loadNewEpisodeData()
        
        //test new episode
        XCTAssertEqual(channelListInteractorInput.newEpisodeList ?? [NewEpisode]() , [NewEpisode]())
        XCTAssertEqual(channelListInteractorInput.newEpisodeList?.count ?? 0 , 0)
    }
    
    func testLoadChannelData(){
        channelListInteractorInput.loadChannelData()
        
        //test channel
        XCTAssertEqual(channelListInteractorInput.channelList ?? [Channel](), [Channel]())
        XCTAssertEqual(channelListInteractorInput.channelList?.count ?? 0 , 0)
    }
    
    func testLoadCategoryData(){
        channelListInteractorInput.loadCategoryData()
        
        //category test
        XCTAssertEqual(channelListInteractorInput.categoryList?.count ?? 0, 0)
        XCTAssertEqual(channelListInteractorInput.categoryList ?? [MindvalleyTest.Category](), [MindvalleyTest.Category]())
    }
    
    func testResetAllChannelData(){
        channelListInteractorInput.resetAllChannelData()
        
        XCTAssertEqual(channelListInteractorInput.newEpisodeList?.count ?? 0, 0)
        XCTAssertEqual(channelListInteractorInput.channelList?.count ?? 0, 0)
        XCTAssertEqual(channelListInteractorInput.categoryList?.count ?? 0, 0)
        
    }
}

//
//  ChannelListEntityTest.swift
//  MindvalleyTestTests
//
//  Created by Mostafizur Rahman on 28/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit
import XCTest
@testable import MindvalleyTest

class ChannelListEntityTest: XCTestCase {
    private var newEpisode:NewEpisode!
    private var channel:Channel!
    private var category:MindvalleyTest.Category!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        setupNewEpisode()
        setupChannel()
        setupCategory()
    }
    
    private func setupNewEpisode(){
        newEpisode = NewEpisode()
        newEpisode.title = Optional("Conscious Parenting")
        newEpisode.channelName = Optional("Little Humans")
        newEpisode.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/5bdbdd0e-3bd3-432b-b8cb-3d3556c58c94.jpg?transform=w_1080")
        newEpisode.type = Optional("course")
    }
    
    private func setupChannel(){
        channel = Channel()
        channel.id = Optional("1")
        channel.title = Optional("Mindvalley Films")
        channel.iconUrl = Optional("https://assets.mindvalley.com/api/v1/assets/eb493421-c048-492b-a471-bed7604e95d6.jpg?transform=w_1080")
        channel.coverUrl = Optional("https://assets.mindvalley.com/api/v1/assets/e819d374-a6d9-4308-bde1-1e145426e546.jpg?transform=w_1080")
        
        let series1 = Series()
        series1.title = Optional("Human Longevity")
        series1.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/a8ef9c6b-cb5c-4551-b57c-baf261e80cc4.jpg?transform=w_1080")
        series1.channelName = Optional("Mindvalley Films")
        
        let series2 = Series()
        series2.title = Optional("Live Your Quest")
        series2.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/e019134c-fdb0-497c-a613-6b9f1c7166a4.jpg?transform=w_1080")
        series2.channelName = Optional("Mindvalley Films")
        
        channel.seriesList = Optional([series1, series2])
        
        let course1 = Course()
        course1.title = Optional("The Barcelona Experiment")
        course1.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/e0f1bba7-2292-44e6-abbc-cf0137706181.jpg?transform=w_1080")
        course1.channelName = Optional("Mindvalley Films")
        
        let course2 = Course()
        course2.title = Optional("Live Your Quest")
        course2.coverPicture = Optional("https://assets.mindvalley.com/api/v1/assets/e019134c-fdb0-497c-a613-6b9f1c7166a4.jpg?transform=w_1080")
        course2.channelName = Optional("Mindvalley Films")
        
        channel.courseList = Optional([course1, course2])
    }
    
    private func setupCategory(){
        category = Category()
        category.name = Optional("Character")
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        newEpisode = nil
        channel = nil
        category = nil
    }

    func testNewEpisode() {
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(newEpisode.title ?? "", "Conscious Parenting")
        XCTAssertNotEqual(newEpisode.title ?? "", "123 arenting")
        
        XCTAssertEqual(newEpisode.channelName ?? "", "Little Humans")
        XCTAssertNotEqual(newEpisode.channelName ?? "", "123 Humans")
        
        XCTAssertEqual(newEpisode.coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/5bdbdd0e-3bd3-432b-b8cb-3d3556c58c94.jpg?transform=w_1080")
        XCTAssertNotEqual(newEpisode.coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/eb493421-c048-492b-a471-bed7604e95d6.jpg?transform=w_1080")
        
        XCTAssertEqual(newEpisode.type ?? "", "course")
        XCTAssertNotEqual(newEpisode.type ?? "", "series")
    }
    
    func testChannel() {
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(channel.id ?? "", "1")
        XCTAssertEqual(channel.title ?? "", "Mindvalley Films")
        XCTAssertEqual(channel.iconUrl ?? "", "https://assets.mindvalley.com/api/v1/assets/eb493421-c048-492b-a471-bed7604e95d6.jpg?transform=w_1080")
        XCTAssertEqual(channel.coverUrl ?? "", "https://assets.mindvalley.com/api/v1/assets/e819d374-a6d9-4308-bde1-1e145426e546.jpg?transform=w_1080")
        
        XCTAssertEqual(channel.seriesList?.count ?? 0, 2)
        XCTAssertEqual(channel.courseList?.count ?? 0, 2)
        
        XCTAssertEqual(channel.seriesList?[0].title ?? "", "Human Longevity")
        XCTAssertEqual(channel.seriesList?[0].coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/a8ef9c6b-cb5c-4551-b57c-baf261e80cc4.jpg?transform=w_1080")
        XCTAssertEqual(channel.seriesList?[0].channelName ?? "", "Mindvalley Films")
        XCTAssertEqual(channel.seriesList?[1].title ?? "", "Live Your Quest")
        XCTAssertEqual(channel.seriesList?[1].coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/e019134c-fdb0-497c-a613-6b9f1c7166a4.jpg?transform=w_1080")
        XCTAssertEqual(channel.seriesList?[1].channelName ?? "", "Mindvalley Films")
        
        XCTAssertEqual(channel.courseList?[0].title ?? "", "The Barcelona Experiment")
        XCTAssertEqual(channel.courseList?[0].coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/e0f1bba7-2292-44e6-abbc-cf0137706181.jpg?transform=w_1080")
        XCTAssertEqual(channel.courseList?[0].channelName ?? "", "Mindvalley Films")
        XCTAssertEqual(channel.courseList?[1].title ?? "", "Live Your Quest")
        XCTAssertEqual(channel.courseList?[1].coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/e019134c-fdb0-497c-a613-6b9f1c7166a4.jpg?transform=w_1080")
        XCTAssertEqual(channel.courseList?[1].channelName ?? "", "Mindvalley Films")
        
        
        XCTAssertNotEqual(channel.id ?? "", "100")
        XCTAssertNotEqual(channel.title ?? "", "Mindvalley Telefilm")
        XCTAssertNotEqual(channel.iconUrl ?? "", "https://assets.mindvalley.com/api/v1/assets/eb493421-c048-492b-a471-bed7604e95d6.jpg?transform=w_10802324")
        XCTAssertNotEqual(channel.coverUrl ?? "", "https://assets.mindvalley.com/api/v1/assets/e819d374-a6d9-4308-bde1-1e145426e546.jpg?transform=w_108023434324")
        
        XCTAssertNotEqual(channel.seriesList?.count ?? 0, 20)
        XCTAssertNotEqual(channel.courseList?.count ?? 0, 20)
        
        XCTAssertNotEqual(channel.seriesList?[0].title ?? "", "Human Longevity 2343")
        XCTAssertNotEqual(channel.seriesList?[0].coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/a8ef9c6b-cb5c-4551-b57c-baf261e80cc4.jpg?transform=w_108056654")
        XCTAssertNotEqual(channel.seriesList?[0].channelName ?? "", "Mindvalley FiTelefilmlms")
        XCTAssertNotEqual(channel.seriesList?[1].title ?? "", "Live Your Telefilm ")
        XCTAssertNotEqual(channel.seriesList?[1].coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/e019134c-fdb0-497c-a613-6b9f1c7166a4.jpg?transform=w_1080fdgdee")
        XCTAssertNotEqual(channel.seriesList?[1].channelName ?? "", "Mindvalley Telefilm")
        
        XCTAssertNotEqual(channel.courseList?[0].title ?? "", "The Barcelona Test")
        XCTAssertNotEqual(channel.courseList?[0].coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/e0f1bba7-2292-44e6-abbc-cf0137706181.jpg?transform=w_10804354632")
        XCTAssertNotEqual(channel.courseList?[0].channelName ?? "", "Mindvalley Telefilm")
        XCTAssertNotEqual(channel.courseList?[1].title ?? "", "Live Your Telefilm")
        XCTAssertNotEqual(channel.courseList?[1].coverPicture ?? "", "https://assets.mindvalley.com/api/v1/assets/e019134c-fdb0-497c-a613-6b9f1c7166a4.jpg?transform=w_1080567657")
        XCTAssertNotEqual(channel.courseList?[1].channelName ?? "", "Mindvalley Telefilm")
    }
    
    func testCategory() {
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(category.name ?? "", "Character")
        XCTAssertNotEqual(category.name ?? "", "Carrer")
    }
}

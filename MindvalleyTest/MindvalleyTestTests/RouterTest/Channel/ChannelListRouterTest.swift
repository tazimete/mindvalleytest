//
//  ChannelListRouterTest.swift
//  MindvalleyTestTests
//
//  Created by Mostafizur Rahman on 29/5/20.
//  Copyright © 2020 Mindvalley. All rights reserved.
//

import UIKit
import XCTest
@testable import MindvalleyTest

class ChannelListRouterTest: XCTestCase {

    func testCreatChannelListRouter(){
        XCTAssertEqual(MockChannelListRouter.isModuleCreated, false)
        
        MockChannelListRouter.creatChannelListModule(channelListViewController: ChannelListViewController())
        
        XCTAssertEqual(MockChannelListRouter.isModuleCreated, true)
    }
    
}

//MARK:
class MockChannelListRouter: ChannelListRouterProtocol {
    public static var isModuleCreated = false
    
    static func creatChannelListModule(channelListViewController: ChannelListViewController) {
        isModuleCreated = true
    }
    
}
